---
subtitle: "Réseau"
tags: [Réseau,Internet, IP, TCP, bit alterné, DNS, sous-réseau]
---

# Réseau

* [Le cours TD partie  1](reseau/réseau-internet.pdf)
* [Vidéo sur l'encapsulation](https://www.youtube.com/watch?v=_0thnFumSdA)
* [Le cours TD partie  2](reseau/réseau-internet2.pdf)

