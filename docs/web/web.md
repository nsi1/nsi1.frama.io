---
subtitle: "Le web"
tags: [HTML,CSS, JavaScript,Flask,Python]
---

# Le Web



## HTML et CSS

* [Cours -TP](web/html-1-html.pdf)
* [Fichier joint au TP](web/html_css.zip)
* [Lien vers l'éditeur de texte notePad ++](https://notepad-plus-plus.org/downloads/) télécharger la dernière  version en 64 bits

## Javascript 

* [Cours-TP](web/html-2-javascript.pdf)
* Le TP se fait en utilisant le navigateur [Firefox](https://www.mozilla.org/fr/firefox/new/) 
* [BD: Javascript n'est pas java](https://coderanch.com/t/456377/a/401/javascript-java.jpg) 

## Un serveur en Python- méthode GET et POST

*  [Cours-TP](web/html-3-client-serveur.pdf)
*  [Templates HTML utilisés dans le TP](web/TP-serveur.html)
*  [Archive contenant le formulaire du TP](web/formulaire.zip)
*  [Le protocole HTTP](https://pixees.fr/informatiquelycee/n_site/nsi_prem_http.html) par David Roche
*  [Les URL](https://pixees.fr/informatiquelycee/n_site/nsi_prem_url.html) par David Roche
*  [Le Web](https://pixees.fr/informatiquelycee/n_site/nsi_prem_web_intro.html) par David Roche
*  [BD: le https](https://blog.octo.com/bd-le-https/)  par Aryana Pezé



