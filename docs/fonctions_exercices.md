#  Exercices sur les fonctions



### Exercice 1

Compléter la fonction  `f` qui a pour paramètre un flottant `x` et qui renvoie le double du carré de `x`. Autrement dit qui correspond à la fonction $f: x \mapsto 2x^2$. 

