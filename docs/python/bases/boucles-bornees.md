## Boucles bornées

Lorsque l'on doit répéter un nombre défini à l'avance de fois la même suite d'instructions, on utilise une boucle bornée ou boucle `for` *pour* en anglais.

La syntaxe Python est:
```python
for i in range(n):
    bloc_instructions_1
bloc_instructions_2
```
La variable `i` va prendre successivement les valeurs 0, 1, 2 jusqu'à $n-1$. Pour chacune des valeurs de `i` le bloc d'instructions `bloc_instructions_1` est exécuté. Ensuite, s'il existe, le bloc d'instructions `bloc_instructions_2` est exécuté.

??? important "Exemple"
    Voici deux programmes qui affichent  quatre fois "Bonjour".

    === "Sans boucle"
        ```python
        print("Bonjour")
        print("Bonjour")
        print("Bonjour")
        print("Bonjour")
        ```
    === "Avec une boucle"
        À l'aide d'une boucle:
        ```python
        for  i in range(4):
            print("Bonjour")
        ``` 

!!! important "Exemple"
    La procédure `table` a pour paramètre un entier et  affiche la table de multiplication de cet entier.
    === "Dans un IDE"

        {{ IDE('python/bases/boucles-bornees-exemple1') }}
        
    ===  "Dans pythonTutor"
    
        <iframe width="800" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=def%20table%28nombre%3A%20int%29%3A%0A%20%20%20%20%22%22%22%20%0A%20%20%20%20Cette%20proc%C3%A9dure%20affiche%20la%20table%20de%20multiplication%0A%20%20%20%20du%20param%C3%A8tre%20nombre%20qui%20est%20un%20entier.%0A%20%20%20%20%22%22%22%0A%20%20%20%20for%20i%20in%20range%2811%29%3A%0A%20%20%20%20%20%20%20%20%23%20i%20prend%20successivement%20les%20valeurs%20de%200%20%C3%A0%2010%0A%20%20%20%20%20%20%20%20print%28i,%22fois%22,%20nombre,%20%22%C3%A9gal%22,%20i%20*%20nombre%29%0Atable%287%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>



## La fonction `#!python range`

Lorsqu'on utilise `#!python for i in range(a, b)`, `i` prend successivement les valeurs de `a` **compris** jusqu'à `b` **non compris** :
=== "Programme"

    ```python
    for i in range(6, 11):
        print(i)
    ```
=== "Résultat"

    Affiche:

    ```python
    6
    7
    8
    9
    10
    ```

Si on ne précise qu'une valeur, cette valeur correspond à `b` et `a` est considéré égal à `0` :

=== "Programme"

    ```python
    for i in range(4):
        print(i)
    ```

=== "Résultat"

    Affiche:

    ```python
    0
    1
    2
    3
    ```

On peut aussi préciser une troisième valeur : le **pas** qui indique de combien on incrémente (augmente) ou décrémente (diminue) `i` à chaque passage dans la boucle. Par exemple pour faire augmenter  `i`  de deux en deux :

=== "Programme"

    ```python
    for i in range(3,10,2):
        print(i)
    ```

=== "Résultat"
    Affiche:

    ```python
    3
    5
    7
    9
    ```

On peut aussi faire diminuer `i` en précisant un pas négatif :

=== "Programme"
    ```python
    for i in range(20,10,-3):
        print(i)

    ```

=== "Résultat"
    Affiche:

    ```python
    20
    17
    14
    11
    ```
## Avec le module tortue

Nous allons utiliser le module turtle *tortue* en anglais qui permet de faire se déplacer une tortue sur l'écran.


Pour l'utiliser vous devez commencer par écrire:

```python
from turtle import *
```

Les commandes dont nous allons avoir besoin sont:

- `forward(pas)` : la tortue avance de `pas` pixels.
- `left(degre)` : la tortue tourne sur place de `degre`  degrés vers la gauche.
- `right(degre)` : la tortue tourne sur place de `degre` degrés vers la droite.



Pour plus  de détails  sur l'utilisation de ce module vous pouvez acceder au [mémo du module turtle](https://perso.limsi.fr/pointal/_media/python:turtle:turtleref.pdf){ .md-button }

!!! note "Exemple"
    Ce programme définie une procédure `carre` qui lors de son appel fait tracer à la tortue un carré de coté 100.

    {{ IDE('python/bases/boucles-bornees-exemple2') }}


## Boucle bornée et accumulateur

On s'intéresse à une fonction `score_mot` qui vérifie la spécification suivante :
- prototype :
    - paramètre : une chaîne de caractères `ch`
    - valeur renvoyée : un nombre entier `s`
    
- préconditions : 
    - `ch` est composée de **quatre** caractères choisis parmi `'a'` ou `'b'` (par exemple `'babb'`

- postconditions :
    - la valeur renvoyée `n` correspond au score de `ch` où chaque `'a'` vaut 1 point et chaque `'b'` vaut 3 points.

### Version n° 1 

Cette fonction `score_mot` utilise quatre variables `s0, s1, s2, s3` qui comptent les points de chacun des quatre caractères (grâce à la fonction `score_lettre`). Puis à la fin on effectue la somme de ces quatre variables pour avoir le score total.

??? note "Version 1"
    {{ IDE('python/bases/boucles-bornees-accumulateur1') }}

Cette méthode ne serait pas généralisable avec un mot `ch` qui aurait un nombre quelconque de caractères (et pas exactement 4 caractères) car elle utilise une variable pour chaque caractère : c'est un défaut majeur.

### Version n° 2 "Courte"

Cette version n’est pas plus générale mais permet d’utiliser moins de variables,
est très courte et tout aussi compréhensible

??? note "Versions 2"

    {{ IDE('python/bases/boucles-bornees-accumulateur2') }}

### Version n°3 avec accumulateur

On utilise l'algorithme naturel que l'on utiliserait à la main et qui utilise un «accumulateur». Par exemple pour calculer le score de `'babb'` de tête on ferait :
- `'b'` : score = 3 points
- `'a'` : score = 3 + 1 = 4 points
- `'b'` : score = 4 + 3 = 7 points
- `'b'` : score = 7 + 3 = 10 points

Lorsqu'on effectue ce calcul de tête on utilise donc un «accumulateur» qui stocke les points au fur et à mesure qu'on lit le mot `'babb'`.  
On peut également utiliser cette méthode avec notre fonction `score_mot`. Ce qui donne alors :

??? note "Version 3"

    {{ IDE('python/bases/boucles-bornees-accumulateur3') }}

Cette façon de coder a deux avantages : 
- d'une part elle traduit simplement ce que nous ferions «de tête»,
- et surtout (ce que nous verrons plus bas) elle permet de mettre en évidence une même instruction répétée quatre fois ce qui va nous être utile pour calculer le score d'un mot ayant un nombre quelconque de caractères.  


!!! important "Remarque"
    Ici la variable `score` est initialisée à 0 puis sert à y accumuler des points qu'on additionne au fur et à mesure. On parle d'un **accumulateur** : c'est un concept qui est très souvent utilisé en programmation (c'est justement parce qu'il est souvent utilisé qu'on lui a donné un nom).

### Version 4


Dans la version précédente, on a donc quatre fois la même instruction avec seule-
ment l’indice du caractère qui varie de 0 (compris) à 4 (non compris). Or tous les
langages de programmation permettent de répéter une instruction avec un in-
dice i qui varie. En python les quatre instructions ci-dessus peuvent ainsi être
remplacées par :

```python
for i in range(4): 
    score = score + score_lettre(ch[i])

```

Ce qui donne:
??? note "Version 4"
    === "Voir dans un IDE"
        {{ IDE('python/bases/boucles-bornees-accumulateur4') }}
    === "Voir dans Pythontutor"
        <iframe width="800" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=def%20score_lettre%28car%3A%20str%29%20-%3E%20int%3A%0A%20%20%20%20if%20car%20%3D%3D%20'a'%3A%0A%20%20%20%20%20%20%20%20return%201%0A%20%20%20%20else%3A%0A%20%20%20%20%20%20%20%20return%203%0A%0Adef%20score_mot%28ch%3A%20str%29%20-%3E%20int%3A%0A%20%20%20%20score%20%3D%200%0A%20%20%20%20for%20i%20in%20range%280,%204%29%3A%0A%20%20%20%20%20%20%20%20score%20%3D%20score%20%2B%20score_lettre%28ch%5Bi%5D%29%0A%20%20%20%20return%20score%0A%0As%20%3D%20score_mot%28'babb'%29%0Aprint%28s%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

Le code est plus court, mais l’intérêt principal est qu’il est facilement généralisable à une chaîne de caractères ch de taille quelconque. Il suffit de faire varier La fonction `#!python range` 
`i` entre `0` et `len(ch)` et pour cela de remplacer `#!python for i in range(0,4):`  par `#!python for i in range(0,len(ch)):`

??? note "Version finale"
    
    {{ IDE('python/bases/boucles-bornees-accumulateur5') }}


Cette amélioration est fondamentale en algorithmique puisque : 

- On travaille souvent sur des données composées de plein d’éléments (ici un mot est composé de plein de caractères) ;
- La taille de ces données est souvent variable.

Une “boucle for” permet donc de faire des algorithmes qui s’adaptent à la taille
des données qui lui sont fournies. Dans cet exemple il est important de comprendre que c’est la démarche naturelle.

À chaque tour de “boucle” :

- On lit un caractère
- On traite l’information ( on retourne 1  si la lettre est “a” et 3 sinon)
- On ajoute cette information à l’accumulateur (on ajoute l’information à l’accumulateur score )

Lorsqu’on « lit » les uns après les autres tous les éléments d’une donnée composée, on dit qu’on « parcourt » cette donnée composée. On peut ainsi :
- parcourir tous les caractères d’une chaîne de caractères;
- parcourir tous les chiffres d’un
nombre;
- parcourir tous les éléments d’une liste, ce que nous verrons plus tard.

Lorsqu’on initialise puis « accumule » plein d’informations dans une même variable, on dit que
cette variable est un « accumulateur ».