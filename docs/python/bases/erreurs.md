# Les messages d'erreurs

Comprendre un message d'erreur d'un programme afin de pouvoir la corriger est une compétence importante pour programmer.

Python signale certaines erreurs en estimant leurs positions et leur type.

Voici quelques erreurs classiques, qu'il est bon de connaitre afin de les corriger rapidement.

## Les erreurs de syntaxe `SyntaxError`

Un programme ne peut être interpréter que s'il respecte la syntaxe du langage dans lequel il est écrit. Lorsque la syntaxe de langage Python n'est pas respectée, l'interpréteur s'arrête et affiche une message d'erreur.

*Syntaxe*  a ici le sens de ne respecter la structure du langage. 

Nous ne listerons qu'une partie des erreurs qui mènent à ce type d'erreur.

### Syntaxe invalide `invalid syntax`
Il y a trois façons d'obtenir une erreur de ce type:

1. Mal orthographier un mot-clé;
2. Oublier un mot-clé;
3. Mal utiliser ou mal placer un mot-clé.

??? important "Exemples"
    === "Exemple 1"

        ```python
        >>> for i range(5):
        File "<input>", line 1
            for i range(5):
                ^
        SyntaxError: invalid syntax
        ```
        Il manque le mot-clé `in`. Le curseur indique le `i` situé juste avant l'oublie.

    === "Exemple 2"

        ```python
        >>> fro i in range(5):
        File "<input>", line 1
            fro i in range(5):
                ^
        SyntaxError: invalid syntax
        ```
        Le mot-clé `for` est mal orthographié. C'est le mot suivant qui est signalé.

    === "Exemple 3"

        ```python
        >>> for i in range(5)
        File "<input>", line 1
            for i in range(5)
                            ^
        SyntaxError: invalid syntax
        ```
        Il manque un `:` qui est indispensable à la fin d'une ligne commençant par `for`. 

    === "Exemple 4"

        ```python
        >>> if 2 + 2 == 4:
                return True
        SyntaxError: 'return' outside function
        ```

        Le mot-clé `return` est mal employé ici car il ne peut être utilisé que dans une fonction.

    === "Exemple 5"

        ```python
        >>> def f(x):
            y = x ** 2
        return y
        SyntaxError: 'return' outside function
        ```
        La ligne contenant le `return` est mal indenté, ce qui fait qu'il n'est plus dans la fonction. Ce qui explique le message d'erreur.

###  `EOL while scanning string literal`
*EOF* est l'acronyme de  *End Of Line*. Littéralement, ce message signifie que la fin de la ligne est atteinte pendant la lecture d'une chaîne de caractères. Il faut comprendre qu'il manque un délimiteur à la chaîne de caractères.  

??? important "Exemple"
    ```python
    >>>"Bonjour
    File "<input>", line 1
    "Bonjour
           ^
    SyntaxError: EOL while scanning string literal"Bonjour
    ```
    Il manque une guillemet après la lettre signalée.

### `cannot assign to function call`
Cette erreur signifie que l'on tente d'affecter une valeur lors de l'appel d'une fonction.
Cela arrive par exemple lorsque l'on utilise des parenthèses à la place de crochets.

??? important "Exemples"

    === "Exemple 1"

        ```python
        >>> tableau = [4, 5]
        >>> tableau(1)  = 7
        File "<input>", line 1
        SyntaxError: cannot assign to function call
        ```
        Ici il faut remplacer les parenthèses par des crochets.

    === "Exemple 2"

        ```python
        >>> def f(x):
        ...     return x ** 2
        >>> f(4) = 16
        File "<input>", line 1
        SyntaxError: cannot assign to function call
        ```
    Il n'est pas possible d'assigner une valeur à l'appel d'une fonction.

## Erreur d'indentation `IndentationError`
Cela signifie que l'indentation est incorrecte. Un bloc peut être pas assez, mal ou trop indenté.

### `expected an indented block`

Un bloc qui devrait être indenté et ne l'est pas. Cela est souvent lorsqu'un bloc suivant une ligne se terminant par unv `:`

??? important "Exemple"

    ```python
    >>> for i in range(5):
    ... print(i)
    File "<unknown>", line 2
        print(i)
        ^
    IndentationError: expected an indented block>>> for i in range(5):... print(i)
    ```
    Il suffit d'indenté la ligne contenant `print(i)` pour corriger cette erreur.

### `unexpected indent`

Un bloc est indenté alors qu'il ne le devrait pas.


??? important "Exemple"

    ```python
    >>> def f(x):
    ...     y = x ** 2
                return y
    File "<unknown>", line 3
        return y
        ^
    IndentationError: unexpected indent
    ```
    La ligne contenant `return` est trop indenté.

### `unindent does not match any outer indentation level`

Un bloc ne correspond à aucun niveau d'indentation.
??? important "Exemple"

    ```python
    >>> for i in range(5):
    ...    print(i)
    ...  print(i  * 2)
    print(i  * 2)
                    ^
    IndentationError: unindent does not match any outer indentation level
    ```

## Erreur de type `TypeError`

### `unsupported operand type(s)`
Un mélange de type n'est pas compatible avec un opérateur.

??? important "Exemple"

    ```python
    >>> a = 2
    >>> b = "3"
    >>> a + b
    Traceback (most recent call last):
    File "<input>", line 1, in <module>
    TypeError: unsupported operand type(s) for +: 'int' and 'str'
    ```
    L'opérateur `+` ne peut fonctionner entre un entier ete une chaîne de caractères.

### `takes ... positional arguments but ... were given`

Une fonction est appelé avec un nombre d'arguments qui ne correspond pas à sa définition.

??? important "Exemple"

    ```python
    >>> def min(a,b):
    ...     return a if a < b else b
    >>> min(3,4,5)
    Traceback (most recent call last):
    File "<input>", line 1, in <module>
    TypeError: min() takes 2 positional arguments but 3 were given
    ```
    La fonction `min` a été appelée avec 3 arguments alors qu'elle n'en prend que 2.

## Erreur de nom `NameError`
Une variable est utilisée alors qu'elle n'a pas été initialisée. Cela arrive souvent lors d'une faute de frappe ou de l'oublie d'un pluriel.

??? important "Exemple"

    ```python
    >>> de1  = 4
    >>> de2 = 6
    >>> des1 + de2
    Traceback (most recent call last):
    File "<input>", line 1, in <module>
    NameError: name 'des1' is not defined
    ```
    La variable `des1` n'est pas définie. Il s'agit certainement d'une faute de frappe sur la variable `de1`.

## Erreur d'index : `IndexError`
Dans un tableau, une chaîne ou un tuple, on tente d'acceder à un indice qui n'est pas défini. Le plus souvent il s'agit d'un indice trop grand.


??? important "Exemples"

    === "Exemple 1"

        ```python
        >>> tab = [4, 5]
        >>> tab[2]
        Traceback (most recent call last):
        File "<input>", line 1, in <module>
        IndexError: list index out of range
        ```
        `tab` est de longueur 2.  Seuls les indices 0 et 1 sont définis. l'indice 2 n'est donc par possible.

    === "Exemple 2"


        ```python
        >>> tab = [4, 5]
        >>> tab[len(tab)]
        Traceback (most recent call last):
        File "<input>", line 1, in <module>
        IndexError: list index out of range
        ```
        `tab` est de longueur 2.  Seuls les indices 0 et 1 sont définis. l'indice `len(tab)` qui est  2 n'est donc par possible.




*Source et inspiration: [https://www.carnets.info/python/erreurs/](https://www.carnets.info/python/erreurs/)* 