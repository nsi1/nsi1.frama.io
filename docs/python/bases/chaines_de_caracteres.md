# Chaines de caractères

Nous avons déjà rencontré les chaînes de caractères. Nous avons vus qu'elles s'écrivaient entre guillemets simples ou doubles que l'on pouvait les concaténer avec l'opérateur `+`, les comparer entre elles, reproduire plusieurs fois une même chaine avec l'opérateur `*`. 
 
 Nous allons approfondir notre connaissance sur ce type.

## Connaitre la longueur `len` d'une chaîne de caractères

La longueur d'une chaîne de caractères,c'est-à-dire le nombre de caractères qu'elle contient, s'obtient grâce à la méthode python `len`  de l'anglais length.

??? note "Exemple"

    ```python
    >>> len('Bonjour')
    7
    >>>  a = "Au revoir"
    >>> len(a)
    9
    ```

## Accéder à un caractère `[ ]` d'une chaine de caractères

Les caractères d'une chaine de caractères sont indexés à partir de zéro et s'obtiennent en indiquant leur numéro d'indice entre crochets.

Les indices vont donc de 0 à la longueur de la chaine moins 1. 

!!! note "Exemple"

    ```python
    >>> s = "abcd"
    >>> s[0]
    'a'
    >>> s[1]
    'b'
    >>> s[2]
    'c'
    >>> s[3]
    'd'
    >>> s[len(s) - 1]
    'd'
    >>> s[7]
    IndexError: string index out of range
    ```
    Dans cet exemple `s` est une chaîne de 4 caractères. Les indices sont  donc compris entre 0 et 3. 3 est la longueur de la chaîne moins un que l'on peut obtenir en saisissant `#!python len(s) - 1`. Demander l'accès à un indice qui n'existe pas lève une erreur `index out of range` soit indice qui sort de la place d'indices possibles.

On peut  utiliser des indices négatifs : `-1` correspond alors au dernier caractère, `-2` à l'avant dernier caractère, etc.

!!! note "Exemple"

    ```python
    >>> s = "abcd"
    >>> s[-1]
    'd'
    >>> s[-4]
    'a'
    >>> s[-9]
    IndexError: string index out of range
    ```
    Les indices négatifs possibles sont compris entre -1 et-4, c'est-à-dire l'opposé de la longueur de  `s` . Demander l'accès à un indice qui n'existe pas lève une erreur

!!! important "Remarque"

    On ne peut pas modifier un caractère d'un chaine:
    ```python
    >>> t = "pale"
    >>> t [1] = 'i'
    TypeError: 'str' object does not support item assignment
    ```
    Ce que l'on peut traduire par les objets de type chaine de caractères ne permettent pas l'affectation d'un de ses éléments.

## Comparaison de chaînes de caractères

Les opérateurs `==` et `!=` renvoient un booléen indiquant si deux chaînes de caractères sont respectivement égales ou différentes :

??? note "Exemple"

    ```python
    >>> "abcd" == 'abcd'
    True
    >>> "abcd" != "abcd"
    False
    >>> "azerty" != 'test'
    True
    ```

On peut aussi comparer **par ordre alphabétique (*lexicographique* pour être rigoureux)** deux chaînes de caractères.  Pour cela on utilise les opérateurs de comparaisons: `<`, `>` , `<=` ou `>=` 

??? note "Exemple"

    ```python
    >>> "bonjour" < "hello"
    True
    >>> "test" >= "tester"
    False
    ```

!!! important "Remarque1"
    Les majuscules sont classées AVANT les minuscules. En effet, l'«alphabet» de python contient tous les caractères imaginables. Pour comparer deux caractères, python utilise donc leur point de code Unicode, nous en parlerons plus tard, qui respecte l'ordre alphabétique MAIS classe les majuscules avant les minuscules.

    ```python
    >>> 'Python' < 'python'
    True
    ```

??? important "Remarque 2"
    L'ordre alphabétique n'est pas l'ordre des nombres entiers.

    ```python
    >>> 100 < 99 # Comparaison entre entiers
    False
    >>> '100' < '99' # Comparaison entre chaînes de caractères
    True
    >>> "3009" < '0110' # Comparaison de dates format JJMM
    False
    >>> "0930" < '1001' # Comparaison de dates format MMJJ
    True
    ```

    Si l'on veut que l'ordre de dates correspondent à l'ordre lexicographique il faut les écrire au format AAAAMMJJ
