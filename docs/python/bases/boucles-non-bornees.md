# Boucles non bornées

## Syntaxe

Les boucles `for` permettent de répéter un nombre défini de fois un bloc d'instructions.

Il y a des cas ou cela n'est pas possible. Dans ce cas, on utilise une boucle `while`, "tant que" en anglais dont la syntaxe est:

```python
while condition:
    bloc_instructions_1
bloc_instructions_2
```

Le mot clé `while`  est suivi d'une condition qui est ponctué par un `:`. S'en suit un bloc d'instructions indenté qui sera répété *tant que* la condition sera vraie.

Le bloc `bloc_instructions_1` est répété  tant que  la condition `condition` est évaluée à `True`.
Dès que la condition `condition` est évaluée à `False` le bloc `bloc_instructions_2`, s'il  existe, est exécuté. 

!!! note "Exemple"

    Ce programme affiche la valeur de affiche la valeur affectée à la variable `i` tant que `i < 5`
    === "Dans un IDE"

        {{ IDE('python/bases/boucles-non-bornees-exemple1') }}


    === "Dans pythonTutor"

        <iframe width="600" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=i%20%3D%201%0Aprint%28'D%C3%A9but'%29%0Awhile%20i%20%3C%205%3A%0A%20%20%20%20print%28i%29%0A%20%20%20%20i%20%3D%20i%20%2B%201%0Aprint%28'Fin'%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

    
    === "Affichage"

        Ce programme affiche:
        ```python
        Début
        1
        2
        3
        4
        Fin
        ```
        La variable `i` prend successivement les valeurs de 1 à 5.

## Attention  aux boucles infinies

Les boucles `#!python while` présente un risque de boucle infinie. En effet, si la condition après le mot clé `#!python while` est toujours  évaluée à `True` la boucle se répète indéfiniment et le programme ne s'arrête pas. Il faut alors forcer son arrêt.

!!! note "Exemple"
    === "Exemple 1"
    
        ```python
        while True:`
            print('Bonjour!')
        ```
        Affiche une infinité de fois `Bonjour!`
    === "Exemple 2"

        ```python
        i = 5
        while i > 3:
            i = i + 1
        ```
        `i` est initialisée à 5 et  à chaque tour de boucle est incrémenté de 1. `i` est donc toujours strictement  supérieur à 3. La condition `i > 3` est donc toujours évaluée à `True` et la boucle est donc répétée indéfiniment. 


## Exemple 2

On place une somme d'argent sur un compte rémunéré à 5 %. Le capital ainsi placé augmente chaque année de 5 %. C’est-à-dire qu’il est multiplié par 1,05 chaque année. On veut connaître le nombre d'années qu'il faut pour faire fructifier une certaine somme d'un montant donné.

Pour cela on va utiliser la fonction `calcul_nb_annees` qui a pour paramètres:

- `capital_depart` un flottant qui est le capital initial déposé.
- `gain` un flottant qui est le gain que l'on souhaite.

Cette fonction retourne un entier  qui représente la première année ou le capital a rapporté au moins le gain escompté.

!!! note "Exemple 2"
    === "Dans un IDE"

        {{ IDE('python/bases/boucles-non-bornees-exemple2') }}

        On doit utiliser une boucle `#!python while` car on ne sait pas à l'avance combien d'années il faut pour obtenir le gain escompter. Obtenir ce gain traduit par la condition `capital_arrivee <= capital` .

        Le bloc d'instruction qui calcule le capital de l'année suivante et incrémente l'année de 1 est répété *tant que* l'on n'a pas cette condition, ce qui se traduit par `capital_arrivee > capital`  

    === "Dans pythonTutor"

        <iframe width="600" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=def%20calcul_nb_annees%28capital_depart%3A%20float,gain%3A%20float%29%20-%3Eint%3A%0A%20%20%20%20capital%20%3D%20capital_depart%0A%20%20%20%20capital_arrivee%20%3D%20capital_depart%20%2B%20gain%0A%20%20%20%20annee%20%3D%200%20%23ICI%0A%20%20%20%20while%20capital_arrivee%20%3E%20capital%3A%0A%20%20%20%20%20%20%20%20capital%20%3D%201.05%20*%20capital%0A%20%20%20%20%20%20%20%20annee%20%3D%20annee%20%2B%201%0A%20%20%20%20%20%20%20%20%23LA%0A%20%20%20%20return%20annee%0A%0Acalcul_nb_annees%28100,%2020%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

    === "Trace de ce programme"

        Voici la trace de l'appel `calcul_nb_annees(100,20)` qui retourne le nombre d'années nécessaire pour que 100€ rapporte 20€. Les nombres ont été arrondis au centième près.

        Le tableau contient l'état de la condition `capital_arrivee > capital` et des variables `capital` et `annee` au  moment des commentaires `#ICI` et `#LA`.

        |      | capital_arrivee > capital | capital | annee |
        | :--: | :-----------------------: | :-----: | :---: |
        | #ICI |             X             |   100   |   0   |
        | #LA  |          `True`           |   105   |   1   |
        | #LA  |          `True`           | 110,25  |   2   |
        | #LA  |          `True`           | 115,76  |   3   |
        | #LA  |          `False`          | 121,55  | **4** |

        Cet appel retourne 4: Il faut 4 ans pour que 100€ placé à 5% d'intérêt rapporte 20€.


## L'instruction `#!python break`

Avec l'instruction `#!python break` nous pouvons arrêter la boucle même si la condition  après le`#!python while` est vraie. Cela permet une sortie anticipée d'une boucle, ce qui peut être utile dans certain cas.
On peut aussi utiliser cette instruction dans une boucle `#!python for`

!!! note "Exemples"
    === "Exemple 1"

        ```python
        i = 1
        while i < 5:
            print(i)
            if i == 2:
                break
            i = i + 1
        print("Fin")
        ```
        Lorsque `i` est affecté de la  valeur 2, le test `i == 2` est évalué à `True`. L'instruction `break` est exécuté et le programme sort de la boucle.

        Ce programme affiche:
        ```python
        1
        2
        Fin
        ```

    === "Exemple 2"

        ```python
        while True:
            a = input()

            if a == "q":
                print("Fin")
                break 
            else:
                print("echo",a)

        ```
        Ce programme demande à l'utilisateur saisir une chaine de caractères au clavier. Si cette chaine est `q` le programme s'arrête et affiche `Fin` sinon il affiche `echo ` suivit de la chaine saisie puis attend à nouveau que une saisie par l'utilisateur.

        [Voir dans Basthon](https://nsi1.frama.io/basthon-console/?script=eJwrz8jMSVUIKSpNteLlUgCCRAVbhcy8gtISDU1eLohQZhpI1FZBqVAJqggECooy80o0lNwy85Q0EaJJRamJ2QoQfmpOcSqmhtTkjHwlnUSgHgAZBBx5)


## L'instruction `#!python continue`
L'instruction `#!python continue` permet de passer au tour de boucle suivant sans sortir de la boucle.

On peut aussi utiliser cette instruction dans une boucle `#!python for`

!!! note "Exemple"

     ```python
     i = 0
     while i < 5:
        i = i + 1
        if i == 2:
            continue # passe au tour suivant si i == 2. Dans ce cas le print(i) n'est pas exécuté.
        print(i)
     ```
     Ce programme affiche:

     ```python
     1
     3
     4
     ```



## `#!python while` ou `#!python for`  ?

On peut toujours remplacer une boucle bornée par une boucle non bornée.

!!! note "Exemples"

    Les deux programmes affichent les nombres de 1 à 5, le premier utilise un boucle bornée, le second une boucle non  bornée.

    === "Exemple 1"

        ```python
        for i in range(1,6):
            print(i)
        ```
    === "Exemple 2"

        ```python
        i = 1
        while i < 6:
            print(i)
            i = i + 1
        ```


Par contre, lors que l'on ne sait pas à priori le nombre de tours de boucle nécessaire, il faut utiliser une boucle `#!python while` . Dans ce cas il faut être vigilant au fait que le programme sort de la boucle.




    
