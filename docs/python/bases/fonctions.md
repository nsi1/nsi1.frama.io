# Les fonctions
Une fonction ou une procédure sont des blocs d'instructions que l'on a nommé et que l'on peut utiliser par la suite  en les rappelant par leur nom.

L'utilisation d'une fonction ou d'une procédure se fait en deux étapes:

-  Sa **définition**: on définie son nom, ses paramètres et la suite d'instructions à exécuter;
-  Son **appel** , *call* en anglais: on utilise la fonction avec des arguments explicites donnés au paramètres.

Nous avons déjà appelé quelques fonctions intégrées, *builtin* en anglais, à Python  comme `type` ou `float`. Vous trouverez [ici](https://docs.python.org/fr/3/library/functions.html) l'ensemble de ces fonctions intégrées.

Un exemple important est la fonction `#!python print` qui permet d'afficher du texte dans la console.

Elle prend en argument des expressions séparées par des virgules. Elle les évalue puis affiche les résultats  séparés par une espace quelque soit le type du résultat puis va à la ligne. Vous trouverez dans la [documentation](https://docs.python.org/fr/3/library/functions.html#print) les options de cette fonction. 

!!! important "Exemple"
    Exécutez le programme suivant:
    {{ IDE('python/fonction_exemple1')}}



## La définition d'une fonction
La syntaxe de la déclaration d'une fonction est la suivante:
```python
def nom_fonction(parametres):
    bloc_instructions1
bloc_instruction2
```
La définition d'une fonction commence par le mot-clé `#!python def` suivie du nom de la fonction.

Les paramètres s'il y en a sont séparés par des virgules en donnant éventuellement leur type. 
La première ligne se termine par un deux-points.
Le contenu de la fonction est le bloc  `bloc_instructions_1` qui est indenté de 4 espaces (ou d'une tabulation).
Il n'est exécuté que lors de l'appel de la fonction. Le bloc `bloc_instructions_2`, s'il existe, est toujours exécuté.

Une fonction renvoie une valeur ou plusieurs valeurs: pour cela on utilise l'instruction `#!python return`. Lorsque l'instruction `#!python return` est exécutée la fonction se termine en renvoyant la valeur de la ou les expressions qui la suive.


!!! important "Exemple"
    
    ```python
    def prix_ttc(ht: float, tva: float) -> float:
        ttc = ht * ( 1 + tva / 100)
        return ttc
    ```
    Cette fonction se nomme `prix_ttc`. Elle a deux paramètres `ht` et `tva`. Le contenu de la fonction contient une première ligne qui affecte et définie une variable `ttc` contient le calcul du prix ttc. La seconde ligne renvoie le prix ttc.
    
    Les *annotations de type* `ht:float` et `-> float` ne sont pas obligatoires mais permettent à l'utilisateur de la fonction de connaitre le type des arguments attendus par celle-ci. 
    
    Pour en savoir plus sur les [annotations de type](https://mypy.readthedocs.io/en/stable/cheat_sheet_py3.html#type-hints-cheat-sheet)

!!! remark "Remarque"
    :warning: Il faut faire la différence entre `return` et `print`.

    - Le mot clé `return`  est suivi de l'expression à retourner par la fonction, mais n'affiche rien. Cela permet, comme dans l'exemple d'affecter la valeur retournée à une variable.
    - La fonction `print` produit un affichage, elle  retourne `None` On ne peut affecter une variable à l'aide d'une fonction `print` 


## Définir une procédure

En Python, une procédure est une fonction que ne contient pas l'instruction `#!python return`. 
Une procédure peut  par exemple réaliser des affichages, des tracés ou écrire dans un fichier. Elle renverra alors implicitement`None`.

!!! important "Exemple"
    ```python
    def affichage(texte:str):
        print('************')
        print('**' + texte + '**')
        print('************')
    ```
    Cette procédure se nomme `affichage` elle a un argument nommé `texte` de type chaîne de caractères. Le bloc d'instructions contient la fonction #!python `print` qui permet d'afficher du texte  dans la console.

## Appel d'une fonction ou d'une procédure
Pour appeler une fonction, il suffit d'utiliser la syntaxe:
```python
nom_fonction(arguments)
```
Une fois une fonction définie, on peut **l'appeler** pour que son bloc d'instructions soit exécuté. Pour cela il faut son nom et donner des valeurs à ses paramètres que l'on appelle alors des **arguments**.

!!! important "Exemple"
    C'est un programme complet.
    Les premières lignes définissent la fonction `prix_tva`.

    La ligne 6  appelle cette  fonction  avec les arguments `ht = 200` et `tva = 20`. Le résultat est affecté à la variable `prix_tva`.

    Ainsi `prix_tva` est affecté de la valeur $200 \times \left( 1 + \frac{20}{100}\right) = 200 \times 1,2$ soit $240$. 

    La dernière ligne affiche `"200 euros hors taxe soit 240.0 euros ttc"`  dans la console.

    === "Voir dans un IDE"
    
        {{ IDE("python/fonction_exemple2")}}
    === "Voir dans pythonTutor"
        <iframe width="800" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=def%20prix_ttc%28ht%3A%20float,%20tva%3A%20float%29%20-%3E%20float%3A%0A%20%20%20%20%20%20%20%20ttc%20%3D%20ht%20*%20%28%201%20%2B%20tva%20/%20100%29%0A%20%20%20%20%20%20%20%20return%20ttc%0A%0Aprix%20%3D%20200%0Aprix_tva%20%3D%20prix_ttc%28prix,20%29%0Aprint%28prix,%22euros%20hors%20taxe%20soit%22,prix_tva,%22euros%20ttc.%22%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

!!! remark "Remarque"
    Les variables définie dans le bloc d'instruction et les noms des paramètres sont locaux à la fonction. On ne peut pas, par exemple, accéder au contenu de la variable `tva` ou à celui de la variable `ttc` en dehors de la fonction `prix_ttc`. Pour plus de détails voir la partie suivante.

Pour vérifier qu'une fonction fait ce qui est attendu, une bonne pratique consiste à faire un jeu de tests.

!!! example "Exemple"

    ```python
    def somme(a,b):
        return a + b
    assert somme(2,2) == 4, "2 + 2 doit être égal à 4"
    assert somme (0,5) == 5, "0 + 5 doit être égal à 5"
    ```
    
    Ce programme n'affichera rien. Les deux premières lignes permettent de définir une fonction `somme` qui renvoie la somme des deux nombres donnés en paramètre. Les deux dernières lignes teste cette fonction.
    
    La syntaxe de l'instruction `#!python assert` est :
    ```python
    assert condition, "message d'erreur"
    ```
    
    - Si la condition est vérifiée, le programme continue;
    - Si la condition n'est pas vérifiée, le message d'erreur s'affiche et le programme s'arrête. La suite du programme ne sera pas exécutée. 

## Notion de variables locales et globales.

Les variables définies à l'intérieur d'une fonction sont dites **locales**.  

Les variables définies à l'extérieur des fonctions sont des variables **globales**. 

Le contenu d'une variable globale est accessible à l'intérieur d'une fonction mais ne peut être modifier. 

Le contenu d'une variable locale n'est accessible qu'à l'intérieur de la fonction dans laquelle elle est définie.

Une bonne pratique de programmation consiste à ne pas modifier dans une fonction une variable globale. Il y a quelques exceptions où cela est nécessaire. Python le permet en utilisant le mot clé `#!python global`.

!!! warning  "Attention"
    L'usage du mot clé `#!python global` est interdit  sauf cas très particulier.


!!! important "Exemples"
    === "Exemple 1"

        ```python
        a = 3 # variable globale
        def test():
            a = 2 #variable locale
            print(a)
        test() # affiche la valeur de la variable locale a
        print(a) # affiche la valeur de la variable globale a
        ```

        Ce programme affiche "2" la valeur associée à la variable locale `a` contenu dans la fonction `test` puis "3" la valeur associée à la variable globale `a`.

        ```python
        2
        3
        ```
    === "Exemple 2"

        ```python
        def test():
            a = 2
            b = 3
        print(a,b)
        ```

        Ce programme retourne l'erreur `#!python NameError: name 'a' is not defined` La variable `a` est locale ainsi elle n'existe que dans la fonction `test` ; elle n'est pas définie lorsqu'elle est appelée par la fonction `#!python print` .

        **Remarque:** Lors du message d'erreur le programme s'arrête.  Il n'y a pas de message d'erreur disant que la variable `b` n'est pas définie alors qu'elle aussi n'est pas définie car locale.
    === "Exemple 3"
        
        ```python
        a = 3 
        def test(a):
            a = 0
            return a + 1
        print(test(a),a)
        ```
        L'affectation `a = 0` dans la fonction remplace la valeur associée à l'argument `a` de la fonction `test`. La variable globale `a` n'a pas été modifiée.

        Ce programme affiche:
        ```python
        1 3
        ```

    === "Exemple 4"

        ```python
        a = 3 
        def test():
            a = a + 1
            return a
        test()
        ```
        Ce programme renvoie une erreur:
        ```python
        UnboundLocalError: local variable 'a' referenced before assignment
        ```
        Dans la fonction `test` la variable `a` est locale. Il n'est pas possible d'y faire référence pour évaluer `a + 1` alors qu'elle n'a pas été définie.
    === "Exemple 5"

         Il est possible d'accéder dans une fonction  à la valeur associée à une variable globale si elle n'est pas modifiée.

        ```python
         a = 3 
        def test():
            b  = a + 1
            return b
        print(test())
        ```
        Ce programme affiche `4`.

    === "Exemple 6"
        
        ```python
        a = 3 
        def test():
            global a
            a  = a + 1
            return a
        print(test(),a)
        ```

        Ce programme affiche:
        ```python
        4 4
        ```
         La variable globale `a` n'est pas un argument de la fonction `test` mais le mot clé `#!python global` permet de la modifier. Dans la fonction la variable globale `a` est associé à 4. Après l'appel de cette fonction, la variable globale `a` est associée à 4.


        
        
    


## Pour approfondir

La [documentation python](https://docs.python.org/fr/3.7/tutorial/controlflow.html#more-on-defining-functions) présente d'autres notions liées aux fonctions  comme les valeur par défaut des paramètres, les paramètres nommés et les listes de paramètres arbitraires.

