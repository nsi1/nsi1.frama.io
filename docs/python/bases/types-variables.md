# Types de base et variables

## Types de base

Les langages de programmation permettent de manipuler des données. Celles-ci sont de natures différentes que l'on appelle **type**. On distingue les types de base qui sont l'objet de ce paragraphe des types structurés qui font l'objet d'autres paragraphes.

En Python, ce sont les nombres, les textes que l'on appellent chaînes de caractères et les booléens.

### Les nombres

Python distingue deux types de nombres.

#### Les entiers (int) 

Les nombres entiers **int** pour "*integer*" en anglais. Par exemple `4, 234478, -6, -924322222`. 

??? note "Remarques"

    - En Python  ces nombres peuvent être aussi grand que l'on veut.
    - Attention à ne pas mettre d'espaces dans un nombre. Bien que les conventions de typographie française recommandent d'écrire mille $1\ 000$, en Python il faut écrire `1000`.

#### Les flottants (float)

Les nombres à virgules flottantes, que l'on va appeler flottants pour "*float*" en anglais. Ils représentent une partie des nombres réels. Pour plus de détails se reporter au chapitre théorique sur ce sujet.  Les conventions utilisées étant américaines, le séparateur décimal est le point.

Par exemple: `3.5, 2.71,-4.0` 

Pour les grands nombres, on peut utiliser la notation scientifique avec la lettre `e` ou `E`.  Ainsi
$7\ 000 = 7 \times 10 ^3$ s'écrit `7e3`  et $-0,001\ 234 = -1,234 \times 10^{-3}$ s'écrit  `-1.234e-3`

??? note "Remarque"
    Attention en Python `3,5` désigne un couple dont les éléments sont 3 et 5 et non le nombre $3,5$. 

### Les chaînes de caractères (str)

Les chaînes de caractères "*string*" en anglais sont des textes écrits entre guillemets simples ou doubles. `"Bonjour"` et `'Bonjour'` représente la même chaine. L'intérêt est que si l'on délimite une chaîne par un type de guillemets on peut utiliser l'autre type de guillemets  dans le texte:

```python
'Il dit: "Il est midi"'
"C'est l'été!"
```

Les chaînes  doivent être sur une seule ligne. Un texte de plusieurs lignes doit être délimité par trois guillemets simples ou doubles.  

```python
"""Et le vent se lève
douloureux rafraichissant
si fantomatique.
"""
```



### Les booléens (bool)

Les données de type **booléen** "*bool*" pour "*boolean*" en anglais ne peuvent prendre que deux valeurs *Vrai* et *Faux* notées `True` et `False` en Python. Attention  à la majuscule au début de ces mots.

Par exemple, si l'on évalue l'expression $5 < 6$  le résultat est `True` car 5 est bien inférieur à 6. 

##  Variables

Les données d'un programme sont associées  à des **variables**. Chaque variable a un nom.  caractères autorisés sont les lettres, les chiffres et le  tiret bas : <kbd>_</kbd> . De plus, un nom de variable ne peut  ni commencer par un chiffre ni être un mot  réservé de Python.
Ce langage  est sensible à la casse, il différencie les majuscules des minuscules. Par exemple `nombre` , `Nombre` et `NOMBRE` sont des variables différentes.

!!! important "Exemples"
    Les noms de variables suivants ne sont pas valides:
    
    - `1ier_joueur` (Commence pas un chiffre)
    - `no-client` ( - non autorisé)
    - `joueur 1` (espace non autorisé)
    - `#!python pass` (mot réservé Python ce qui est visible pas sa coloration)



### Affecter une valeur

Pour stocker une valeur dans une variable, on utilise une instruction appelé **affectation** notée `=`. Par exemple:

```python
temperature =  21
```

!!! important  "Attention"
    * Le signe `=`  n'a pas le sens d'égalité comme en mathématiques. On peut utiliser l'image d'un casier étiqueté `temperature` qui contiendrait le nombre 21 et le type `int`. 
    * Si au cours du programme une nouvelle affectation de cette variable a lieu, alors elle est modifiée et prend cette nouvelle valeur et le type est mis à jour. 
    * Si la variable apparait ailleurs qu'à gauche dans une affectation alors elle représente sa valeur.

Une **expression** est une combinaison de valeurs, variables, opérateurs, et de fonctions qui sont évaluées  par le langage. Elle fournit une valeur et son type qui peut être stocké dans une variable.

`#!python 4 + 5` est une expression de type entier dont la valeur est `#!python 9`. 
Lors de l'exécution de `#!python a =  4 + 5`, l'expression `#!python 4 + 5` est d'abord évaluée puis son résultat est affecté à la variable `a`. Ainsi `a` **référence** la valeur `#!python 9` et le type `#!python int`.

**Exemple**

```python
>>> prix_au_kilo = 4.5 
>>> prix_au_kilo
4.5
>>> prix_deux_kilos = prix_au_kilo  * 2
>>> prix_deux_kilos
9.0
>>> prix_au_kilo = 5.1
>>> prix_au_kilo
5.1
>>> prix_deux_kilos
9.0
```
La variable  `prix_au_kilo` est affecté de la valeur `4.5`.  La deuxième ligne permet d'afficher le contenu de cette variable. L'affectation de la variable `prix_deux_kilos` correspond au double de la valeur de la variable `prix_au_kilo` soit $4,5 \times 2 = 9$ .
La ligne suivante permet d'afficher ce résultat. La variable `prix_au_kilo` est affecté d'une nouvelle valeur `5.1`. La dernière ligne montre que cela n'a pas modifié la valeur affectée à la variable  `prix_deux_kilos`. 

## Les fonctions `#!python type` et `#!python isinstance`
La fonction `#!python type` prend en argument une expression et renvoie le type de cette expression.

**Exemple**

```python
>>> type(2)
int
>>> type(2.0)
float
>>> type('2')
str
>>> type(False)
bool
>>> type(6/3)
float
>>> type(5 + 2 * 0. 5)
float
```
!!! warning "Attention"
    Comme le montre les deux derniers exemples:
    
    - La division entre deux entiers renvoie un flottant;
    - Dans une expression qui mélange des entiers et des flottants, la valeur renvoyée est un flottant.

La fonction `#!python isinstancce` permet de savoir si une expression est d'un certain type. 
Elle prend en argument une expression et un type et renvoie un booléen.
Si l'expression est du type demandé elle renvoie `True` sinon elle renvoie `False`.

**Exemple**

```python
>>> isinstance(5,int)
True
>>> isinstance(5,float)
False
>>> isinstance(5.4,str)
False
>>> isinstance('2'+'2',int)
False
```

## Conversion entre types - transtypage

Il existe des fonctions qui permettent de convertir, lorsque cela est possible, une expression dans un autre type.

| Fonction | Utilité                                  | Exemple                                                      |
| :-------- | :---------------------------------------- | :------------------------------------------------------------ |
| int()    | convertit en entier                      | `int('12')` prend en argument la chaîne de caractères '12' renvoie le nombre entier 12 |
| float()  | convertit en nombre flottant | `float('3.1415926')` prend en argument une chaîne de caractères et  renvoie le nombre flottant 3.1415926 |
| str()    | convertit en chaîne de caractères        | `str(8.2)` prend un flottant en argument  renvoie la chaîne de caractères '8.2' |

**Exemples**

```python
>>> 'La température est de '+ str(20) + ' degrés Celsius'
'La température est de 20 degrés Celsius'
>>> 'La température est de '+ 20 + ' degrés Celsius'
TypeError: can only concatenate str (not "int") to str
>>> int("3")
3
>>> int(4.5)
4
>>> int("4.5")
ValueError: invalid literal for int() with base 10: '4,5'
```
!!! warning "Attention"
    Comme le montre les deux derniers exemples:
    
    - Convertir un flottant en un entier renvoie sa partie entière;
    - Convertir une chaîne de caractères en un  entier n'est pas toujours possible. Par contre, `#!python int(float('4.5'))` qui converti d'abord en un flottant puis en un entier renvoie `4`.




