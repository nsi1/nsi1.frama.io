# Les modules
Presque tous les langages permettent d'avoir des fonctions supplémentaires en utilisant des bibliothèques. En Python on appelle ces bibliothèqyes des **modules**.

Python est particulièrement riche en modules. Il y a des modules pour la gestion du hasard, pour le traitement de données, le traitement d'images, les interfaces graphiques, l'intelligence artificielle, etc. 

Un **module** est un fichier dans lequel sont définies des fonctions et des constantes. Pour pouvoir l'utiliser, il faut d'abord l'importer dans Python à l'aide du mot clé `import` suivi du nom du module.

!!! important "Exemple"
    Le module `random`, *hasard* en anglais, permet de simuler le hasard.

    Ce programme va simuler le lancer de deux dés et afficher le résultat de ce lancer. 

    {{ IDE("python/bases/modules-exemple1")}}

    - `import random` permet d'importer le module `random` dans le programme. Les fonctions  contenues dans ce module sont accessibles en utilisant la notation `nom_module.nom_fonction`. Il en est de même pour les constantes.  
    - `random.randint(1,6)` permet l'appel de la fonction `randint` du module `random` .  Cette fonction a deux paramètres qui sont des entiers. Ici elle est appelé avec pour argument les nomtre 1 et 6, elle va retourner un entier compriis entre 1 et 6

!!! important "Un autre exemple"

    ```python
    >>> import math
    >>> math.sqrt(2)
    1.41421356237
    >>> math.pi
    3.14159265359
    ```

    - `import math`  permet d'importer le module `math` dans la mémoire de l'interpréteur.
    - `math.sqrt(2)`appelle la fonction `sqrt`du module `math` avec l'argument `2`. Cet appel retourne une valeur approchée de $\sqrt{2}$ .
    - `math.pi` appelle la constante `pi`du module `math`. Cet appel retourne une valeur approchée de $\pi$. 



Pour éviter de préfixer par le nom du module et quand on n'a besoin que de certaines fonctions ou constantes d'un module, on peut n'importer que ce qui est nécessaire en utilisant la syntaxe suivante:
```python
from nom_module  import liste_fonctions_constantes
```

!!! important "Exemple"
    L'exemple précédent peut s'écrire:

    ```python
    >>>from math import sqrt , pi
    >>>sqrt(2)
    1.41421356237
    >>>pi
    3.14159265359

    ```

    Seules la fonction `sqrt`et la constante `pi` ont été importées du module `math`. Les autres fonctions et constantes du modules maths ne sont pas accessibles. 

??? remark "Remarque"

    On pourrait aussi utiliser la syntaxe `from math import *` dans ce cas toutes les fonctions et constantes sont importées dans le programme. Le `*`veut ici dire tout. C'est une mauvaise pratique et donc déconseillé.  En effet, s'il y a deux fonctions qui ont le même nom nous ne pouvons savoir  quelle fonction l'interpréteur va choisir.

On peut aussi créer un alias pour le nom du module afin d'avoir un nom plus court à saisir. La syntaxe est:
```python
import nom_module as nouveau_nom
```

!!! important "Exemple"
    En reprenant toujours le même exemple:
    ```python
    >>> import math as m
    >>> m.sqrt(2)
    1.41421356237
    >>> m.pi
    3.14159265359
    ```

    La ligne `import math as m` permet d'importer le module `math` et de choisir comme préfixe `m`. Cela aurait pu être n'importe quel autre préfixe, mais il faut mieux être le plus clair possible.


## Pour aller plus loin

Pour connaître le contenu d'un module, vous pouvez soit:

- Consulter la documentation en ligne qui est partiellement traduite;
-  Utiliser la fonction `help` qui affiche une aide en anglais.

??? important "Exemple" 

    ```python
    >>> import random as rd
    >>> help(rd.randint)
    Help on method randint in module random:

    randint(a, b) method of random.Random instance
        Return random integer in range [a, b], including both end points.


    ```