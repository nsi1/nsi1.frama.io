# Opérateurs

Une expression permet de calculer une valeur en les combinant entre elles d'autres valeurs  par des **opérateurs**.  Dans l'expression $2 + 3$, 2 et 3 sont les **opérandes** de l'opérateur $+$. Les principaux opérateurs sont les opérateurs arithmétiques, logiques et de manipulation de chaînes.

## Opérateurs arithmétiques

!!! important  "Opérateurs arithmétiques"

    === "+"
    
        **Addition**
    
        ```python
        >>> 2 + 5
        7
        >>> a = 15
        >>> b = 2
        >>> a + b
        17
        ```


    === "-"
        **Soustraction**

        ```python
        >>> 2 - 5
        -2
        >>> a = 5
        >>> b = -2
        >>> a - b
        7
        ```
    === "*"
        **Multiplication**

        ```python
        >>> 2 * 5
        10
        >>> a = 5
        >>> b = -2
        >>> a * b
        -10
        ```
    
    === "/"
        **Division**

        ```python
        >>> 5 / 2
        2.5
        >>> a  = 10
        >>> b = 2
        >>> a / b
        5.0
        ```
    === "**"
        **Exposant**
        
        L'expression `a ** b` équivaut à l'expression mathématiques $a^b$ 
        ```python
        >>> 5 ** 2
        25
        >>> a = 2
        >>> b = 3
        >>> a ** b
        8
        ```
    === "%"
        **Modulo**
        
        L'expression `a % b` retourne le reste de la division euclidienne de `a` par `b`. 
        
        * Dans 7 il y a 3 fois 2 et il reste 1;
        * Dans 8 il y a 4 fois 2 et il reste 0.
    
        Nous avons donc:
        ```python
        >>> 7 % 2 
        1
        >>> a = 8
        >>> b = 2
        >>> a % b
        0
        ```
    
    === "//"
        **Quotient entier**
        
        L'expression `a // b` retourne le quotient de la division euclidienne de `a` par `b`.
        
        * Dans 7 il y a 3 fois 2 et  il reste 1;
        * Dans 8 il y a 4 fois 2 et il reste 0.
    
        Nous avons donc:
        ```python
        >>> 7 // 2 
        3
        >>> a = 8
        >>> b = 2
        >>> a // b
        2
        ```

L'ordre des priorités opératoires est le même qu'en mathématiques. 

[Notebook sur les opérateurs](/basthon-notebook/?from=/python/notebooks/python-calculs.ipynb)

## Opérateurs de comparaison

Ces opérateurs compare deux  opérandes et retournent le booléen `True` si les opérandes vérifient la relation et `False` sinon.
!!! important "Opérateurs de comparaison"
    === "=="
        **Egalité**

        ```python
        >>> 2 * 5 == 15 - 5
        True
        >>> 2 + 2 == 5
        False 
        ```
    === "!="
        **Différence**

        ```python
        >>> 2 != 5
        True
        >>> 2 + 2 != 4
        False
        ```
    === ">"
        **Supérieur à**

        ```python
        >>> 2 > 5
        False
        >>> 9 - 2 > 4
        True
        ```
    === ">="
        **Supérieur ou égal à**

        ```python
        >>> 5 >= 5
        True
        >>> 9  >= 4
        True
        ```
    === "<"
        **Inférieur à**

        ```python
        >>> 2 < 5
        True
        >>> 9 - 2 < 4
        False
        ```
    === "<="
        **Inférieur ou égal  à**

        ```python
        >>> 2 <= 5
        True
        >>> 4 <= 4
        True
        ```
    
## Opérateurs logiques


!!! important "Opérateurs logiques"
    === "and"
        **ET logique**


        Retourne  `True` uniquement si les deux opérandes sont vérifiés.
        ```python
        >>> (2 < 3) and (4 < 5)
        True
        >>> (4 == 5) and ( 2 + 2 == 4)
        False
        >>> (2 + 3 == 5) and (1 != 1)
        False
        ```

        La table de vérité de l'opérateur `and`

        | `a` | `b` | `a and b` |
        |:---|:--:|:--:|
        | `False` | `False`| `False` |
        | `False` | `True` | `False` |
        | `True` | `False` | `False` |
        | `True` | `True` | `True` |
    
    === "or"
        **OU logique**
    
        Retourne `False` uniquement si les deux opérandes ne sont pas vérifiés. Autrement dit, retourne `True` si l'un, l'autre ou les deux opérandes sont vérifiés.
        ```python
        >>> (2 == 1 + 1) or (3 != 3)
        True
        >>> (5 != 5) or (3 != 3)
        False
        >>> (3 == 5) or (7 == 7)
        True
        ```

        
        La table de vérité de l'opérateur `or`
        
        | `a` | `b` | `a or b` |
        |:---|:--:|:--:|
        | `False` | `False`| `False` |
        | `False` | `True` | `True` |
        | `True` | `False` | `True` |
        | `True` | `True` | `True` |
    
    === "not"
        **NON logique**
    
        C'est un opérateur unitaire car il n'y a qu'un opérande.
    
        Il retourne `True` si l'opérande est évalué à `False` et `False` sinon.
        ```python
        >>> not( 4 == 2 + 2)
        False
        >>> not (5 == 4)
        True
        ```

        La table de vérité de l'opérateur `not`

        | `a` | `not(a)` |
        |:--:|:--:|
        | `False` | `True` |
        | `True` | `False` |


??? note "Une erreur classique"

    ```python
    a = 4
    if a == 2 or 3:
        print('a égal 2 ou 3')
    else:
        print('a différent de 2 et 3')
    ```

    Ce programme affiche `'a égal 2 ou 3'`  alors que `a = 4`. 

    Avant d'expliquer ce qui se passe en Python, l'erreur se situe dans la langue. En français, "a est égal à 2 ou 3" équivaut à la phrase plus lourde par ses redondances "a est égal à 2 ou a est égal à 3". La version correcte est donc:
    
    ```python
    a = 4
    if a == 2 or a == 3:
        print('a égal 2 ou 3')
    else:
        print('a différent de 2 et 3')
    ```
    Affiche bien `'a différent de 2 et 3'`. 

    Dans Le premier  cas  `a == 2` est évaluée  à `False` mais l'entier 3 est interprété comme `True`. La structure conditionnelle est évaluée à `True` ce qui explique le résultat. 
    C'est une particularité lié à Python, dans d'autres langages le résultat pourrait être différent.

    Une autre façon d'obtenir le résultat est d'utiliser une liste.
    ```python
    a = 4
    if a in [2,3]:
        print('a égal 2 ou 3')
    else:
        print('a différent de 2 et 3')
    ```
    Affiche bien `'a différent de 2 et 3'`. 

??? note  "Le caractère paresseux des  opérateurs `and` et `or`"

    Le caractère paresseux des opérateurs `or` et `and` signifie que lors de l’évaluation d’une expression logique de type `(A and B)`, l’expression A est d’abord évaluée, mais l’expression B n’est évaluée que si le résultat de l’évaluation de A est égal à `True` . En effet, si A a été évalué à `False`, l’expression `(A and B)` sera elle aussi évaluée à `False`, quelle que soit l’évaluation de B.
    Pour des raisons analogues, lors de l’évaluation de l’expression `(A or B)` l’expression A est d’abord évaluée, puis l’expression B mais uniquement dans le cas où A aura été évaluée à `False`.

    === "Un exemple pour comprendre"

        Voyons cela sur un exemple: 
        ```python
        def faux():
            print("Dans faux")
            return False

        def vrai():
            print("Dans vrai")
            return True

        print('Test et 1')
        print(faux() and vrai())
        print("Test et 2")
        print(vrai() and faux())
        print('Test et 3')
        print( vrai() and vrai())

        print('Test ou 1')
        print(faux() or vrai())
        print('Test ou 2')
        print(vrai() or faux())
        print('Test ou 3')
        print(vrai() or vrai())
        ```

        Ce programme affiche:
        ```python
        Test et 1
        Dans faux
        False
        Test et 2
        Dans vrai
        Dans faux
        False
        Test et 3
        Dans vrai
        Dans vrai
        True
        Test ou 1
        Dans faux
        Dans vrai
        True
        Test ou 2
        Dans vrai
        True
        Test ou 3
        Dans vrai
        True
        ```
    
    === "Une application de ce caractère paresseux"

        Voici une fonction qui recherche un élément dans une liste et renvoie le premier indice correspondant à cet élément. 
        
        ```python
        def recherche(elem, L):
            i = 0
            while L[i] != elem or i < len(L):
                i = i + 1
            return i
        ```

        Ne fonctionne comme attendue car lorsque l'élément recherché n'est pas dans la liste on a:

        ```python
        >>> recherche(5,[2,3,1,4])
        IndexError: list index out of range
        ```

        L'opérateur `or` de par son caractère paresseux évalue `L[i] != elem` à `True` à chaque tour de boucle car `elem` n'appartient pas à la liste `L`. `i` est donc toujours incrémenté jusqu'à ce qu'elle dépasse la longueur du tableau et provoque cette erreur. 

        En modifiant l'ordre dans le test, on obtient la fonction :

        ```python
        def recherche(elem, L):
            i = 0
            while i < len(L) or L[i] != elem:
                i = i + 1
            return i
        ```

        On a maintenant:

        ```python
        >>> recherche(5,[2,3,1,4])
        4
        ``` 

        Cette fonction ne lève plus d'erreur car `i <len(L)` est évalué à chaque tour de boucle et est évaluée à `False` dès que `i >= len(L)`. La valeur retournée correspond à la longueur de la liste lorsque l'élément recherché n'est pas dans la liste. Il est facile de ne rien renvoyer si l'élément n'est pas présent. Par exemple ainsi:

        ```python
        def recherche(elem, L):
            i = 0
            while i < len(L) or L[i] != elem:
                i = i + 1
            return i
            if i < len(L):
                return i
        ```



        


## Opérateurs d'appartenance
Les opérateurs d'appartenance de Python sont `in` et `not in`.  Ils permettent de savoir si une valeur est présente, ou non, dans une séquence (chaîne, liste, tuple, dictionnaire, etc.)

!!! important "Opérateurs d'appartenance"
    === "`in`"
        `a in b` retourne `True  si l'élément `a` est dans la séquence `b`.
        ```python
        >>> "a" in "La nuit."
        True
        >>> 5 in [0, 2, 4, 6 ,8]
        False
        ```
    === "`not in`"
        `a not in b` retourne `True  si l'élément `a`n'est pas  dans la séquence `b`.
        ```python
        >>> "a" not in "Le jour."
        True
        >>> 5 not in [1,3,5,7,9]
        False
        ```
    
## Priorités des opérateurs

??? note
    L'ordre décroissant des priorités des opérateurs est:

    -  `()`
    - `**`
    - `*, /, //, %`
    - `+, -`
    - `==, !=, >, >=, <, <=`
    - `in, not in`
    - `not`
    - `and`
    - `or`

## Opérateurs sur les chaînes de caractères

Il y a deux opérateurs de manipulation des chaînes de caractères `+` et `*`.


!!! important "Opérateurs sur les chaînes de caractères"
    === "+"
        **Concaténation**
        
        L'opérateur `+` permet joindre plusieurs chaînes et d'en créer une nouvelle. On dit que l'on **concatène** des chaînes.
        ```python
        >>> "abc" + "def"
        'abcdef'
        ```
    
    === "*"
        **Répétition d'une chaîne**
        
        L'opérateur `*` permet de répéter une chaîne de caractères.
        ```python
        >>> 'ab' * 3
        'ababab'
        ```

On peut comme pour les nombres utiliser les opérateurs de comparaison sur les chaînes de caractères. Cependant les miniscules sont différentes des majuscules qui sont toutes avant les minuscules. `'A' < 'Z' < 'a' < 'z'`.

**Exemples**
```python
>>> "bonjour" < "hello"
True
>>> "Un" == "un"
False
>>> "bonjour" < "Bonjour"
False
```

!!! warning "Attention"
    Les opérateurs `+` et `*` doivent être utilisé entre opérandes du même type. Si vous tenter de comparer une chaine de caractères avec un nombre vous obtiendrez une erreur de type *TypeError* en anglais.
    
    ```python
    >>> "a" < 2
    TypeError: '<' not supported between instances of 'str' and 'int'
    ```

