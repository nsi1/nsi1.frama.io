# Exercices

## TD

- [TD calculs et variables](td/TD-NotionVariablesCalculs.pdf)
- [TD Opérateurs de comparaison et logiques](td/TD_booleens.pdf)
- [TD notions sur fonctions](td/TD-notionsFonctions.pdf)
- [TD variables locales et globales](td/TD_variables_locales_globales.pdf)
- [TD structures conditionnelles](td/TD-structuresConditionnelles.pdf)
- [TD chaines de caractères](td/TD-chainesDeCaracteres.pdf)
- [TD boucles for](td/TD_boucle_for.pdf)
- [TD boucles while](td/TD-bouclesWhile.pdf)

## TP

- [TP notions sur fonctions](tp/TP_premièresFonctions.pdf)
- [TP structures conditionnelles](tp/exercices2structuresConditionnelles.pdf)

## Exemple

Un premier programme complet

* [Fiche organisation d'un programme exemple](tp/nombreMystere.pdf)
* [Accès au programme d'exemple](/basthon-console/?from=/python/notebooks/nombreMystere.py)