# Les structures conditionnelles

Les instructions conditionnelles permettent de **sélectionner le bloc d'instructions à exécuter** selon la valeur d'une **expression booléenne**.

## La structure si ... alors

!!! note "Exemple"

    Par exemple, si le prix d'entrée dans un musée dépend de l'âge on pourrait avoir la règle:

    **si** l'âge est supérieur à 16 ans **alors** le prix du billet est 13 €.
    On a, en français, une structure du type **si**  ...**alors** ....
    En Python cela donne:

    ```python
    if age  > 16:
        prix = 13
    ```

De façon plus générale la syntaxe est: 
!!! important "À retenir"

    ```python
    if condition:
        bloc_instructions_1

    bloc_instructions_2   
    ```
    Une instruction conditionnelle commence par le mot-clé `if` , ("si" en anglais) , suivie d'une condition  puis de `:`. Le bloc d'instruction qui suit est indenté.

    `condition` est une expression booléenne qui retourne `True` ou `False`. 

    Si `condition` est évalué à `True` alors le bloc `bloc_instructions_1`   e est exécuté, sinon il est ignoré.

    Dans tous les cas le bloc `bloc_instructions_2` est exécuté.



## La structure si ... alors ... sinon ...

!!! note "Exemple"

    **si** l'âge est supérieur à 16 ans **alors** le prix du billet est 13 €.

    **sinon** le billet est gratuit

    ```python
    if age  > 16:
        prix = 13
    else:
        prix = 0
    ```

Plus généralement, la syntaxe en Python est:
!!! important "À retenir"

    ```python
    if condition:
        bloc_instructions_1
    else:
        bloc_instructions_2
    bloc_instructions_3

    ```

    Où `condition` a pour valeur `True` ou `False`.

    - Lorsque `condition` a pour valeur `True`, le bloc `bloc_instructions_1` est exécuté, et le bloc `bloc_instructions_2` est ignoré.
    - Lorsque `condition` a pour valeur `False`, le bloc `bloc_instructions_1` est ignoré, et le bloc `bloc_instructions_2` est exécuté.

    On exécute donc soit le bloc `bloc_instructions_1`, soit le bloc `bloc_instructions_2`, mais jamais les 2.

    Dans tous les cas, si le bloc `bloc_instructions_3` existe, il est exécuté ensuite.

    Ne pas oublier le `:` après le mot clé `else`. 


## Structure si ... sinonsi ... sinon

!!! note "Exemple"

    **si** l'âge est supérieur à 18 ans **alors** le prix du billet est 13 €.

    **sinon si** l'âge est supérieur à 16 ans **alors** le prix du billet est 9 €.

    **sinon** le billet est gratuit

    On programme une fonction `prix_billet` qui qui va calculer le prix du billet en fonction de l'âge. 

    L'âge doit être positif. On obtient:

    ```python
    def prix_billet(age: int) -> int:
        assert age >=0, "L'age doit être positif"
        if age >= 18:
            prix = 13
        elif age >= 16:
            prix = 9
        else:
            prix = 0
        return prix
    ```

    Que l'on peut tester avec le jeu de tests suivant:

    ```python
    assert prix_billet(3) == 0
    assert prix_billet(17) == 9
    assert prix_billet(18) == 13
    assert prix_billet(34) == 13
    assert prix_billet(56) == 13
    ```

La syntaxe générale en Python est:

!!! important "À retenir"

    ```python
    if condition1:
        bloc_instructions_1
    elif condition2: 
        bloc_instructions_2
    else:
        bloc_instructions_3
    bloc_instructions_4
    ```

    Le mot-clé `elif` est la contraction de `else` et de `if` ce que l'on peut traduire par "sinonsi", il est suivi d'une condition et de `:`  et se poursuit par un bloc indenté.  Il peut y avoir plus qu'un`elif` après un `if`. 

    où `condition1` a pour valeur `True` ou `False`. De même pour `condition2`.

    - Lorsque `condition1` a pour valeur `True`, le bloc `bloc_instructions_1` est exécuté, et les blocs `bloc_instructions_2`  et `bloc_instructions_3` sont ignorés.
    - Lorsque `condition1` a pour valeur `False`:
        -  Le bloc `bloc_instructions_1` est ignoré;
        - Lorsque `condition2` a pour valeur `True`, le bloc `bloc_instructions_2` est exécuté et le bloc `bloc_instructions_3` est ignoré;
        - Lorsque `condition2` a pour valeur `False`, le bloc `bloc_instructions_2` est ignoré et le bloc `bloc_instruction_3` est exécuté.


    On exécute donc un seul des blocs soit le bloc `bloc_instructions_1`, soit le bloc `bloc_instructions_2` soit le bloc `bloc_instructions_3` mais toujours un seul parmis les 3.

    Dans tous les cas, si le bloc `bloc_instructions_4` existe, il est exécuté ensuite.


!!! note "Exemple"

    On peut maintenant écrire une fonction `prix_billet` plus complexe suivant les règles:

    * Si l'âge est entre 16 et 18 ans (exclus) ou si l'âge est supérieur à 65 le prix est de 9 €.
    * Sinon si l'âge est entre 18 ans et 65 ans (exclus) le prix est de 13 €.
    * Sinon l'entrée est gratuite.

    Cela donne:

    ```python
    def prix_billet(age: int) -> int:
        if (age >= 16 and age < 18) or age >= 65:
            prix = 9
        elif age >= 18 and age < 65:
            prix = 13
        else:
            prix = 0
        return prix
    ```

    Puis effectuer le jeu de tests suivant:

    ```python
    assert prix_billet(3) == 0
    assert prix_billet(17) == 9
    assert prix_billet(18) == 13
    assert prix_billet(34) == 13
    assert prix_billet(65) == 9
    assert prix_billet(80) == 9
    ```



## Compréhension fine des expressions booléennes

Quelques exemples pour écrire un code élégant et plus compréhensible.

**Exemple 1**

Voici le code de la fonction `est_majeur`  qui  prend en paramètre un âge et renvoie `True` si l'âge est supérieur ou égal à 18 et `False` sinon 

```python
def est_majeur(age):
    if age >= 18:
        return True
    else:
        return False

```

Ce code est correct mais peut être amélioré ainsi;

```python
def	est_majeur(age):
    return age >= 18
```

*Explication:* L'expression `age >= 18` est une expression booléenne qui renvoie `True` si la variable `age` est affectée d'une valeur supérieur ou égale à 18 et `False` sinon.

**Exemple 2**

Voici le code d'une fonction `est_pair` qui prend en paramètre un nombre et renvoie `True` si le nombre est pair (c'est-à-dire si le reste de la division par 2 de ce nombre est 0) et `False` sinon

```python
def est_pair(n):
    if n % 2 == 0:
        return True
    else:
        return False
```

 Ce code est correct mais il peut être amélioré ainsi:

```python
def est_pair(n):
	return n % 2 == 0
```

*Explication* l'expression booléenne `n % 2 == 0` renvoie `True` si le reste de la division par 2 de `n`  est 0 et `False`  sinon. 



On peut aussi l'alléger l'écriture des jeux de tests.

Ce jeu de tests :

```python
assert est_pair(10) == True
assert est_pair(7) == False
assert est_pair(24) == True
assert est_pair(57) == False

```

Peut s'écrire:

```python
assert est_pair(10) 
assert not est_pair(7) 
assert est_pair(24)
assert not est_pair(57)
```

