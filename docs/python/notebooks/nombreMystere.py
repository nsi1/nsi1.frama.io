# -*- coding: utf-8 -*-
# Modules

import random as rd

# Fonctions

def jeu(n):
    assert n >=1 and n <= 100, "Le nombre est compris entre 1 et 100"
    if n == nb_mystere:
        msg = "Gagné"
    elif n > nb_mystere:
        msg = "Plus petit"
    else:
        msg = "Plus grand"
    return msg


# Variables globales

nb_mystere = rd.randint(1,100)
gagne = False

# Programme principal

# Tant que gagne = False
while not (gagne):
    print("Devinez le nombre mystère. Il est compris entre 1 et 100.")
    # On demande au joueur son choix
    # On verifie qu'il saisi bien un  nombre
    # La méthode isnumeric renvoie True si une chaîne de caractères
    # est un nombre et False sinon. Elle n'est pas à connaître.
    choix_joueur = ""
    while not choix_joueur.isnumeric():
        print("Entrer un nombre:")
        choix_joueur = input()
    choix_joueur = int(choix_joueur)
    message = jeu(choix_joueur)
    print(message)
    #Si le message est "Gagné" la variable gagne = True et le jeu se 
    # termine.
    if message == "Gagné":
        gagne = True
    


