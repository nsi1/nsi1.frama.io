
# Exercices

# TD
 - [QCM listes 1](td/QCM_listes_1.pdf)
 - [QCM listes 2](td/QCM_listes_2.pdf)
 - [QCM listes 3](td/QCM_listes_3.pdf)
 - [TD Parcours listes](td/TD-ParcoursListes.pdf)
 - [QCM tuples](td/QCM_tuples.pdf)
 - [TD tableaux et tuples](td/TD_TableauxTuples.pdf)
 - [TD tableaux par compréhension](td/TD-tableaux-comprehension.pdf)
 - [TD dictionnaires](td/TD_dictionnaires.pdf)
# TP

- [TP Json et API](/basthon-notebook/?from=/python/types/tp/TP_JSON_API.ipynb&aux=/python/types/tp/exemple_json.json&aux=/python/types/tp/exemple_json2.json)