## Dictionnaires


Les données de type `dict`,  également présentes dans d’autres langages sous le nom de "mémoires associatives "ou de "tableaux associatifs" sont des ensembles non ordonnés d’objets auxquels on accède à l’aide d’une clé.

### Définition

##### Définition
Un *dictionnaire* est un ensemble non ordonné de couple clé:valeur. 

**Remarque:**

- Un dictionnaire n'est pas indexé par des nombres contrairement aux séquences comme les listes et les tuples. 
Les clés peuvent être de n'importe quel type tant qu'il est immutable. On peut donc choisir comme clé les chaînes de caractères, les nombres ou les tuples, mais pas les listes.
- Les dictionnaires sont *mutables*, comme les tableaux, on peut modifier les valeurs, en ajouter et en retirer.
- Le plus souvent les clés sont de chaînes de caractères ce qui explique l'appellation de dictionnaire.


!!! example ""
    Pour représenter des données par exemple un élève par son nom, prénom et classe on peut utiliser un dictionnaire.

    - `"nom"` : la chaîne de caractères représentant le nom;
    - `"prenom"`  : la chaîne de caractères représentant le prénom;
    - `"classe"`  : la chaîne de caractères représentant la classe.


    ```python
    >>> eleve = {'nom': 'Dupont','prenom':'Jean','classe':'1GT4'}
    >>> eleve['nom']
    'Dupont'
    >>> type(eleve)
    dict
    ```

Pour obtenir la valeur associée à une clé, il suffit de mettre le nom de la clé entre crochets, si la clé n'existe pas une erreur `#!python KeyError` est renvoyée. . Dans notre exemple:

!!! example ""

    ```python
    >>> eleve['classe']
    '1GT4'
    >>> eleve['anneeNaissance']
    KeyError: 'anneeNaissance'
    ```



### Créer un dictionnaire

Il existe plusieurs façons de créer un dictionnaire. 

- Un dictionnaire vide  en utilisant la syntaxe `{}` ou `dict()`
- Par extension en indiquant entre accolades les couples clé/valeur séparés par des virgules.
- Par compréhension en expliquant entre accolades comment ce dictionnaire est construit.
   - À partir d'un itérable la syntaxe est `{clé(elem) : valeur(elem) for elem in itérable}`. Ou `clé` et `valeur` sont des fonctions de `elem` et `itérable` un objet itérable. 
   - À partir de deux itérables la syntaxe est `{clé: valeur for clé:valeur in zip(itérable_clés,itérable_valeurs)}`
- Par transtypage sur un itérable donc les éléments contiennent deux éléments. 

!!! example ""

    ```python
    un_dictionnaire = {} # un dictionnaire vide
    un_dictionnaire = dict() # un dictionnaire vide
    # Un dictionnaire initialisé par extension
    un_dictionnaire_3 = {'nom': "Dupond", "prénom": "Jean", "annéeNaissance": 2003} 
    # Le dictionnaire {1 : 1, 2: 4, 3: 9} qui au nombres 1,2 et 3 associe leur carré. 
    un_dictionnaire_4  = {x: x **2 for x in range(1,4)} 
    # Le dictionnaire {'nom': 'Adams', 'prenom': 'John} par compréhension
    un_dictionnaire_5 = {cle:valeur for cle:valeur in zip(['nom','prenom'],['Adams','John'])}
    # Le même dictionnaire par transtypage d'un tableau de tuples:
    un_dictionnnaire_6 = dict([('prenom','nom'),('John','Adams')])
    ```

###   Modifier une valeur
Pour modifier une clé existante, il suffit d'affecter à cette clé une nouvelle valeur. Pour affecter à une nouvelle valeur à la clé `'cle'` du dictionnaire `dico` il faut saisir : `#!python dico['cle'] = nouvelle_valeur` 

!!! example ""

    ```python
    >>> eleve = {'nom': 'Dupont','prenom': 'Jean','classe': '1GT4'}
    >>> eleve 
    {'nom': 'Dupont','prenom': 'Jean','classe': '1GT4'}
    >>> eleve['classe'] = '1GT2'
    >>> eleve
    {'nom': 'Dupont', 'prenom': 'Jean', 'classe': '1GT2'}
    ```

### Ajouter une clé et sa valeur associée
Pour ajouter un couple clé valeur à un dictionnaire existant, il suffit de faire comme lorsque l'on modifie une valeur.

!!! example ""

    ```python
    >>> eleve = {'nom': 'Dupont','prenom': 'Jean','classe': '1GT4'}
    >>> eleve['specialites'] = ('maths','NSI',"SVT")
    >>> eleve
    {'nom': 'Dupont','prenom': 'Jean','classe': '1GT4', 'specialites': ('maths','NSI',"SVT") }
    ```


!!! remark "Remarque"

    Lorsque l'on tente de créer une clé de type mutable comme par exemple un tableau, une erreur `#!python TypeError: unhashable type: 'list'` est levée. 

    ```python
    >>> d = dict() # Créer un dictionnaire vide
    >>> d[[1,2]] = 3 # Tente de créer une clé de type tableau à laquelle on affecte la valeur 3.
    TypeError: unhashable type: 'list'
    ```

### Effacer une clé et sa valeur

####  Avec la commande `#!python del`

La commande `del d[cle]` permet d'effacer le  couple clé/valeur correspondant à la clé `cle` du dictionnaire `d`, si cette cké n'existe pas une erreur `#!python KeyError` est renvoyée. 

!!! example ""

    ```python
    >>> eleve = {'nom': 'Dupont','prenom': 'Jean','classe': '1GT4', 'specialites': ('maths','NSI',"SVT") }
    >>> del eleve['specialite']
    >>> eleve
    {'nom': 'Dupont','prenom':'Jean','classe':'1GT4'}
    >>> del eleve['dateNaissance']
    KeyError: dateNaissance
    ```

### Avec la méthode `#!python pop`

La méthode `#!python pop` permet de suprimer une clé d'un dictionnaire et renvoie la valeur suprimée. 
Si la clé n'existe pas une erreur `#!python KeyError` est renvoyée..

!!! example ""

    ```python
    >>>  eleve = {'nom': 'Dupont','prenom': 'Jean','classe': '1GT4', 'specialites': ('maths','NSI',"SVT") }
    >>> spe = eleve.pop('specialites') # Suprime la clé 'specialites' et retourne la valeur associée.
    >>> eleve # La clé 'specialites' a bien été suprimée. 
    {'nom': 'Dupont', 'prenom': 'Jean', 'classe': '1GT4'}
    >>> spe # contient les valeurs de la clé suprimée
    ('maths', 'NSI', 'SVT')
    >>> eleve.pop('dateNaiss') # La clé 'dateNaiss' n'existe pas, une erreur de clé est renvoyée. 
    KeyError: 'dateNaiss'
    ```

### Connaitre le nombre de couple clé/valeur d'un dictionnaire avec la fonction `len`

!!! example ""

    ```python
    >>> eleve = {'nom': 'Dupont','prenom': 'Jean','classe': '1GT4'}
    >>> len(eleve)
    3
    ```

### Tests
On peut utiliser les opéateurs `==` et `!=` ainsi que `in`  et `not in` avec les dictionnaires. 

!!! example ""

    ```python
    >>>  eleve = {'nom': 'Dupont','prenom': 'Jean','classe': '1GT4'}
    >>> 'nom' in eleve # 'nom' est une clé de ce dictionnaire.
    True
    >>> 'Dupont' in eleve # "Dupont" n'est pas une clé de ce dictionnaire.
    False
    >>> 'dateNaissance' in eleve
    False
    >>> eleve != {'nom': 'Dupond','prenom':'Arthur', 'classe': '1GT4'}
    True
    ```
### Copie d'un dictionnaire

Comme pour les tableaux, affecter un dictionnaire revient créer un alias. 

!!! example ""

    ```python
    a ={}
    a['test'] = 4
    b = a
    a['test'] = 7
    print(b)
    ```


Affiche `{'test': 7}`

On constate que lorsque l’on modifie le dictionnaire `a` cela modifie aussi le dictionnaire `b`.

Pour éviter ce problème, on doit utiliser la méthode `copy` de l’objet dictionnaire.

!!! example "" 

    ```python
    a ={}
    a['test'] = 4
    b = a.copy()
    a['test'] = 7
    print(b)
    ```

Affiche` {'test': 4}`

On constate que lorsque l’on modifie le dictionnaire `a`, cela ne modifie pas le dictionnaire `b`. Ces deux dictionnaires sont biens distincts.

## Parcours d'un dictionnaire 

En Python, on peut parcourir un dictionnaire à l’aide d’une boucle `for`. Depuis Python 3.7, les clés sont parcourues dans leur ordre de création, avant cette version l'ordre n'était pas garanti.

###   Parcourir les clés d'un dictionnaire

Comme pour un tableau on peut énumérer les *clé*s d'un dictionnaire à l'aide d'une boucle `for`. Il revient au même d'utiliser la méthode `keys()`.

!!! example "Exemple" 

    ```python
    >>> informaticien  = {'nom':"Von Neumann",'prenom':"John",
                        'anneeNaiss':1903,'anneeMort':1957}
    >>> for cle in informaticien:
        print(cle,informaticien[cle])
    nom Von Neumann
    prenom John
    anneeNaiss 1903
    anneeMort 1957
    >>> for cle in informaticien.keys():
        print(cle,informaticien[cle])
    nom Von Neumann
    prenom John
    anneeNaiss 1903
    anneeMort 1957
    ```

### Parcourir les valeurs d'un dictionnaire avec la méthodes  `values()`

La méthode `values()` appliquée à un dictionnaire renvoie un itérateur sur les valeurs que l'on peut parcourir avec une boucle `for`. 

!!! example "Exemple"

    ```python
    >>> informaticien  = {'nom':"Von Neumann",'prenom':"John",
                        'anneeNaiss':1903,'anneeMort':1957}
    >>> for valeur in informaticien.values():
            print(valeur)
    Von Neumann
    John
    1903
    1957
    ```

On peut créer un tableau ou un tuple à partir de cet itérateur:

!!! example "Exemple"

    ```python
    >>> informaticien  = {'nom':"Von Neumann",'prenom':"John",
                            'anneeNaiss':1903,'anneeMort':1957}
    >>> list(informaticien.values())
    ['Von Neumann','John', 1903, 1957]
    >>> tuple(informaticien.values())
    ('Von Neumann','John', 1903, 1957)
    ```

### Parcourir les paires (clé,valeur ) avec la méthode `items()`

La méthode `items()` appliquée à un dictionnaire renvoie un itérateur sur les couples (clé, valeur) qui peut être parcouru avec une boucle `for`. 

!!! example "Exemple"

    ```python
    >>> informaticien  = {'nom':"Von Neumann",'prenom':"John",
                            'anneeNaiss':1903,'anneeMort':1957}
    >>> for cle, valeur in informaticien.items():
            print("Clé",cle,"Valeur",valeur)
    Clé nom Valeur Von Neumann
    Clé prenom Valeur John
    Clé anneeNaiss Valeur 1903
    Clé anneeMort Valeur 1957
    ```



### Exemple: compter le nombre d'occurrences

Le terme *occurrence* indique le nombre de répétitions d'un mot ou d'une expression dans un texte. Ici il va indiquer le nombre de répétition de chacun des nombres dans un tableau

Il s'agit d'écrire  une fonction `occurence` qui prend en paramètre une liste de nombres et qui retourne un dictionnaire qui a pour clé chacun des nombres contenu dans la liste à laquelle on associe le nombre de fois où ce nombre est présent dans la liste.

!!! example "Compter le nombre d'occurrences"

    ```python
    >>> occurence([1,2,3,3,3,2,1,1,1])
    {1: 4, 2: 2, 3: 3}
    ```

1 est représenté quatre fois dans la liste, 2 deux fois et 3 trois fois

L'algorithme est le suivant:
 Créer un dictionnaire vide puis  parcourir séquentiellement le tableau.  Si le nombre est dans le dictionnaire on ajoute 1 à sa valeur associée sinon on créer une clé pour ce nombre à laquelle on associe 1

 **Remarque** : Dans cette exemple les clés sont des nombres.

!!! example ""
    === "Code"

        ```python
        def occurence(liste):
            occ = {}
            for nombre in liste:
                if nombre in occ:
                    occ[nombre] = occ[nombre] + 1
                else:
                    occ[nombre] = 1
            return occ
        print(occurence([1,2,3,3,3,2,1,1,1]))
        ```

        Ce programme affiche:`{1: 4, 2: 2, 3: 3}`. Il y a bien quatre 1, deux 2 et 3 trois dans ce tableau.

    === "Visualiser dans PythonTutor"

        <iframe width="800" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=def%20occurence%28liste%29%3A%0A%20%20%20%20occ%20%3D%20%7B%7D%0A%20%20%20%20for%20nombre%20in%20liste%3A%0A%20%20%20%20%20%20%20%20if%20nombre%20in%20occ%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20occ%5Bnombre%5D%20%3D%20occ%5Bnombre%5D%20%2B%201%0A%20%20%20%20%20%20%20%20else%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20occ%5Bnombre%5D%20%3D%201%0A%20%20%20%20return%20occ%0A%0Aprint%28occurence%28%5B1,2,3,3,3,2,1,1,1%5D%29%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>


        [Visualiser dans PythonTutor](http://pythontutor.com/visualize.html#code=def%20occurence%28liste%29%3A%0A%20%20%20%20occ%20%3D%20%7B%7D%0A%20%20%20%20for%20nombre%20in%20liste%3A%0A%20%20%20%20%20%20%20%20if%20nombre%20in%20occ%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20occ%5Bnombre%5D%20%3D%20occ%5Bnombre%5D%20%2B%201%0A%20%20%20%20%20%20%20%20else%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20occ%5Bnombre%5D%20%3D%201%0A%20%20%20%20return%20occ%0A%0Aprint%28occurence%28%5B1,2,3,3,3,2,1,1,1%5D%29%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)



