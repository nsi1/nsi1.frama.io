# Les tuples

Les tuples ressemblent aux tableaux, mais que contrairement à eux, ils sont *immutables*. Une fois créé, il n'est plus possible de modifier leurs éléments ou d'en ajouter. C'est très pratique pour stocker des données  qui peuvent être de types différents.

### Définition par extension: 
!!! example ""

    ```python
    # Un tuple contenant 3 éléments
    film = ('Taxi Driver', 'Martin Scorsese', 1976)
    # Un tableau de tuples
    films = [('Swordfish', 'Dominic Sena', 2001), ('Snowden', ' Oliver Stone', 2016), ('Taxi Driver', 'Martin Scorsese', 1976)]
    ```



En Python, un tuple se définie à l’aide de valeurs entre parenthèses séparées par des virgules. Son type est `tuple`.


!!! example ""

    ```python
    >>> A= (1,2)
    >>> B = (2,1)
    >>> A == B # l'ordre est important
    False
    >>> type(A)
    tuple
    ```

On peut créer un tuple sans utiliser de parenthèses, mais leur présence facilite la lecture.


!!! example ""

    ```python
    >>> t = 3, True
    >>> t
    (3, True)
    ```

On peut créer un tuple vide ou un tuple ne contenant qu'un élément.


!!! example ""

    ```python
    a = () # tuple vide
    b = tuple() #tuple vide
    c = (3,) # ne pas oublier la virgule
    ```
### Définition par compréhension

La syntaxe est similaire à celle utilisée les tableaux. Il faut entourer l'expression par `tuple(...)`.

!!! example ""

    ```python
    >>> T = tuple(i ** 2 for i in range(1,4)) # carré des nombres de 1 à 3
    >>> T
    (1, 4, 9)
    >>> T2 = tuple( x * 2 for x in 'abc') # duplique les caractères de la chaîne 'abc'
    >>> T2
    ('aa', 'bb', 'cc')
    >>> T3 = tuple( elem for elem in [2,3,5,6,7] if elem % 2 == 0) # Les éléments pairs de la liste.
    >>> T3
    (2,6)
    ```

### Définition par transtypage

!!! example ""

    ```python
    >>> T = tuple('abc')
    >>> T
    ('a', 'b', 'c')
    >>> T2 = tuple([1,2,3])
    >>> T2
    (1, 2, 3)
    ```

### Accéder aux éléments

 Comme pour les tableaux, on peut accéder aux éléments par leur indice. L'indice commence, comme pour les tableaux, à 0. La fonction `len` retourne le nombre d'éléments.


!!! example ""

    ```python
    >>> test = ('a','b','c')
    >>> test[0]
    'a'
    >>> len(test)
    3
    ```

Si l’on tente de modifier un élément d’un tuple, une erreur `'tuple' object does not support item assignment` est retournée. 

C'est une erreur de type, car le type tuple est immutable. Il ne permet  ni la modification d'un élément ni l'ajout d'un élément.
On ne peut modifier un tuple qu'en l'assignant à nouveau.


!!! example ""

    ```python
    >>> test = ('a','b','c')
    >>> test[1] = 'd'
    TypeError: 'tuple' object does not support item assignment
    >>> test = (1,2,3)
    >>> test
    (1,2,3)
    ```

On peut **concaténer** des tuples:


!!! example ""

    ```python
    >>> a = (1,2) + (3,4,5)
    >>> a
    (1,2,3,4,5)
    ```


!!! remark "Remarque"

    Concaténer des  tuples est moins couteux  que pour des tableaux. 

On peut **disperser** des tuples. Ce qui est très pratique pour réaliser des affectations multiples.


!!! example ""

    ```python

    >>> x,y,z = (1,2,3)
    >>> x
    1
    >>> y
    2
    >>> z
    3
    ```
### Tests

On peut comparer les tuples avec les opérateurs `==` et `!=`

!!! example ""

    ```python
    >>> (1, 2, 3) == (1, 2, 3)
    True
    >>> (1,2,3) != (1, 3, 2)
    True
    ```

On peut déterminer si un élément appartient ou non a un tuple avec `in` et `not in`

!!! example ""

    ```python
    >>> 4 in (1, 2, 3)
    False
    >>> 1 in (1, 2, 3)
    True
    >>> 7 not in (1, 2, 3)
    True
    ```
### Parcours de tuples

Les tuples sont itérables comme pour les listes, ainsi on peut parcourir les tuples par indice ou par élément. 

!!! example ""
    === "Parcours par indice"

        ```python
        T = ('a', 'b', 'c')
        for i in range(len(T)):
            print(T[i], end=" ")
        ```

        Ce programme affiche `a b c`.

    === "Parcours par élément"

        ```python
         T = ('a', 'b', 'c')
        for elem in T:
            print(elem , end=" ")
        ```

        Ce programme affiche aussi  `a b c`.
        ```

### Fonctions et méthodes

Il existe de nombreuses fonctions et méthodes sur les tuples. Seules quelqu'unes seront présentées.

#### Fonctions
Pour les tuples contenant uniquement des nombres, c'est-à-dire du type `int` ou `float`. 
Les fonctions `sum`, `min` et `max` retournent respectivement la somme, le minimum et le maximum des éléments. 

!!! example "" 

    ```python
    >>>T = (1, 5, 2)
    >>>sum(T)
    8
    >>> min (T)
    1
    >>> max(T)
    5
    ```


### Méthodes
Les tuples étant immutables, il n'y a pas méthode similaire  à la méthode  `append` pour les listes. 

Les méthodes `count(elem)` et `index(elem)`  retournent respectivement le nombre d'éléments et le premier indice de l'élément `elem` dans un tuple. 

!!! example ""

    ```python
    >>> T = (1, 2, 5, 6, 2, 8)
    >>> T.count(2) # Retourne le nombre de 2 dans le tuple T
    2
    >>> T.index(6) # Retourne l'indice du premier élément 6
    3 
    ```


## Ce que vous connaissiez déjà sur les tuples sans le savoir:

- **Faire un échange entre deux variables** 
    
!!! example ""
    ```python
    >>> a = 4
    >>> b = 6
    >>> a,b = b,a # On peut écrire avec ou sans parenthèses.
    >>> print(a,b)
    6 4
    ```

  

- **Lorsqu'on souhaite retourner plusieurs valeurs dans une fonction** : le tuple peut être un type de données utilisé comme valeur de retour.


!!! example ""

    ```python
    def min_max(a,b):
        """
        Prend en entrée deux nombres et renvoie un couple
        contenant en premier le plus petit des deux et 
        en second le plus grand
        """
        if a > b:
            return (b,a)
        else:
            return (a,b)
    a = min_max(7,5)
    print(a[0],a[1]) # affiche "5 7"
    m, M = min_max(11,3)
    print(m,M) # affiche "3 11"
    ```


**Pour aller plus loin:** 

[https://docs.python.org/fr/3/tutorial/datastructures.html](https://docs.python.org/fr/3/tutorial/datastructures.html) 

