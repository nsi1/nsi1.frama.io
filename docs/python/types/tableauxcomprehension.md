# Tableaux par compréhension

Une particularité du langage Python est de proposer une  syntaxe courte et élégante pour créer un tableau à partir d'un  itérable, pour nous cela sera un tableau, une chaine de caractères ou l'utilisation de la fonction `range`.

En plus d'être synthétique, cette syntaxe est souvent plus rapide à l'exécution. Dans certains cas, elle jusqu'à deux fois plus rapide pour produire le même tableau.

## Syntaxe  `[ exp for x in t ]`

### En utilsant un tableau existant.

!!! example "Exemple"

    ```python
    T1 = [1, 2, 3, 5]
    print(T1)
    T2 = [2 * x for x in T1]
    print(T2)
    ```
    Le tableau `T2` est construit par compréhension. 
    Ce programme affiche:

    ```python
    [1, 2, 3, 5]
    [2, 4, 6, 10]
    ``` 
    Le tableau `T2` contient les doubles des éléments du tableau `T1` 


La syntaxe générale est:
!!! important ""

    `[expr for x in t]`

où:

- la paire de crochets, les mots-clefs `for` et `in` sont obligatoires ;
- `t` est un itérable, c'est-à-dire un tableau, une chaine de caractères ou l'utilisation  de la fonction `range` ;
- `x` est l’élément courant qui parcourt`t` ;
- `expr` expression qui dépend en général de `x` et dont la valeur est placée dans le tableau.

####  Comparaison compréhension  et boucle `for` 

Une liste en compréhension peut être remplacée par une boucle `for` mauis dans ce cas le code obtenu est plus long. 

!!! example "Exemple"

    Le premier exemple:

    ```python
    T1 = [1, 2, 3, 5]
    T2 = [2 * x for x in T1]
    print(T2)
    ```
    Pourrait être écrit  avec un boucle `for`de la façon suivante:

    ```python
    T1 = [1, 2, 3, 5]
    T2 = []
    for x  in T1:
        T2.append(2 * x)
    print(T2)
    ```
    Dans les deux cas l'affichage serait:
    ```python
    [2, 4, 6, 10]
    ``` 


### En utilisant un itérable

#### En utilisant une chaine de caractères

!!! example "Exemple"

    ```python
    chaine = "test"
    T = [ c * 2 for c in chaine]
    print(T)
    ```
    Ce programme  affiche:

    ```python
    ['tt', 'ee', 'ss', 'tt']
    ```
    Il a créé un tableau dont les éléments sont les caractères de `chaine` répété deux fois.

#### En utilisant la fonction `range`

!!! example "Exemple"

    ```python
    T = [ x ** 2 for x in range(1,11)]
    print(T)
    ```
    Affiche  les carrés de 1 à 10:
    ```python
    [1, 4, 9,  16,  25,  36,  49,  64, 81, 100]
    ```

Il est possible d'appeler une fonction lors de la création d'un tableau par compréhension.
!!! example "Exemple"

    ```python
    def f(x):
        return 4 * x - 7
    T = [f(x)  for x in range(1,11)]
    ```
    `T` contient les images des nombres entiers de 1 à 10 par la fonction définie sur $\mathbb{R}$ par $f(x) = 4x-7$ 

!!! example "Exemple"

    Créer un sous tableau en sélectionnant que certains indices

    ```python
    T  = [3, 5, 7, 9, 11]
    indice = [1,4 ]
    T2 = [T[i] for i in indice]
    ```
    `T2 = [5, 11]` car  il est le tableau contenant les éléments d'indice 1 et 4 de `T`.


## Filtrer des valeurs `[ expr for x in t if  expr2]`

- `expr2` est une expression qui dépend de `x`
- Cette syntaxe permet de sélectionner les éléments de `t` qui vérifie `expr2` 

!!! example "Exemple"

    ```python
    T = [1, 8, 12, 5, 15,  7]
    T2 = [  x for x in T if  x >= 10]
    print(T2)
    ```
    Affiche

    ```python
    [12,15]
    ```
    `T2` contient les éléments `x`  de `T1`  tels que `x >= 10`.

!!! remark "Remarque"
    
    On aurait pu écrire ce code en une ligne:
    ```python
    print([x for x in [1, 8, 12, 5, 15,  7] if x >= 10 ])
    ```

Le  code équivalent avec une boucle `for`  est:
!!! example  ""

    ```python
    T = [1, 8, 12, 5, 15,  7]
    T2 = []
    for elem in  T:
        if elem >= 10:
            T2.append(elem)
    print(T2)
    ```

##  `[expr1 if expr_bool else expr2 for x in t]`

- `expr_bool` est une expression booléenne qui dépend de `x`
- Si `expre_bool` est évaluée à `True` alors `expr1` est dans le tableau, sinon c'est `expr2` 

!!! example "Exemple"

    ```python
    T = [3,6,2,8,9]
    T2 = ["Pair" if x  % 2 == 0 else "Impair" for x in T]
    ```
    `T2 = ["Impair", "Pair", "Pair", "Pair", "Impair"]`.
    `T2` contient `"Pair"` aux indices ou les éléments de `T`  sont pairs et `"Impair"` aux autres indices.
## Tableaux par compréhension de deux itérables

Il est posssible de créer un tableau par compréhension  à partir de deux itérables.

!!! example "Exemple"

    ```python
    T = [x + y for x in 'AB' for  y in  '123']
    print(T)
    ```
    Ce programme affiche:

    ```python
    ['A1',  'A2',  'A3',  'B1', 'B2', 'B3']
    ```
    Le parcours ce fait en prenant un élément de la première chaine `'AB'` puis en le concaténant avec chacun des éléments de la seconde chaine.

L'équivalent avec des boucles `for` serait:

!!! example ""
    ```python
    T  = []
    for x in  'AB':
        for  y in '123':
            T.append(x + y)
    print(T)
    ```

## Tableaux à deux dimensions par compréhension

Il est facile d'initialiser un tableau à deux dimensions par compréhension.

!!! example ""

    ```python
    T  = [[ 0 for j in range(3)] for  i in  range(2)]
    print(T)
    T[1][2] = 1
    print(T)
    ```
    Affiche
    ```python
    [[0, 0, 0], [0, 0, 0]]
    [[0, 0, 0], [0,0, 1]]
    ```

L'indice `i` parcourt les lignes et l'indice `j` parcourt les colonnes. 

L'équivalent avec une boucle `for` est:
!!! example ""

    ```python
    T = []
    for i in range(2):
        ligne  = []
        for j in range(3):
            ligne.append(0)
        T.append(ligne)
    print(T)
    ```

!!! remark "Remarque"
    Il est possible d'imbriquer plus de deux tableaux, mais dans ce cas le code devient difficilement lisible. 



