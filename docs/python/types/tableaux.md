# Les tableaux

Pour éviter de déclarer trop de variables et regrouper des variables qui sont liées, on utilise un tableau. Ainsi au lieu d'écrire:

!!! example ""

    ```python
    ville1 = "Paris"
    ville2 = "Bordeaux"
    ville3 = "Lyon"
    ```

On préfèrera regrouper ces données dans un tableau.

!!! example ""

    ```python
    villes = ["Paris", "Bordeaux", "Lyon" ]
    ```

En Python, il n'existe pas de type "tableau" le type le plus proche est "liste" qui n'a pas d'équivalent strict dans la plupart des autres langages.

!!! example ""

    ```python
    >>> villes = ["Paris", "Bordeaux", "Lyon" ]
    >>> type(villes)
    list
    ```
    La variable `villes` est bien de type `list`. 



## I Créer un tableau

Les tableaux en Python s'écrivent entre crochets et les éléments y sont ordonnés et séparés par des virgules. Une liste vide s'écrit par une paire de crochets.

### A Création par extension

Il suffit de mettre directement les éléments entre crochets.


!!! example "Exemple"
    === "Exemple 1"

        Une liste en extension de quatre entiers:

        ```python
        L1 = [1,4, 7, 5]
        ```

        Une liste vide:

        ```python
        L2 = []
        ```

    === "Exemple 2"

        Ce programme simule le lancé d'un dé bien équilibré à 6 faces et affiche le résultat ainsi que la face du dé obtenu.
        {{ IDE('python/type/tableau_exemple1')}}






Il est possible de créer une liste en extension contenant des types différents:

!!! example ""
    ```python
    L3 = [1,2,True,"Hello",34]
    ```

On considère que cela n'est pas une bonne pratique. Un tableau sert pour des données de même type sur lesquelles on veut faire des traitements, comme déterminer le maximum ou les trier.  Pour mélanger les types, on préfèrera utiliser les types  t-uples  et dictionnaires qui seront vus plus tard.

### B Création par compréhension

Elle est définie en indiquant entre crochets comment la définir. C'est très pratique pour créer une liste qu'il serait fastidieux d'écrire à la main. 

!!! example ""
    ```python
    L4 = [i ** 2 for i in range(1,11)]
    ```
    `L4` contiendra la liste des carrés de 1 à 10. 


Voir [ici](tableauxcomprehension.md) pour plus de détails. 

### C Création par transtypage

Elle est définie en appliquant la fonction `list` sur certains type de données comme une chaîne de caractères. 

!!! example ""
    ```python
    L5 = list('abc')
    L6 = list()
    ```
    `L5` contiendra la liste `['a', 'b', 'c']` 

    `L6` construit une liste vide `[]`


Plus bas, vous trouverez que l'on peut aussi  créer une liste en ajoutant des éléments à la fin en utlisant la méthode `.append`.


### D  Connaitre la longueur d'un tableau


Pour connaitre la longueur, c'est-à-dire le nombre d'éléments, d'une liste,  on utilise la fonction `len`. 

!!! example "Exemple"

    ```python
    >>>len([1,2,3,4])
    4
    >>> t = [5,3,1,4,2]
    >>> len(t)
    5
    ```

### E Tests
Les tests `==`, `!=`, `in` et `not in` sont disponibles pour le tableaux 

Ainsi:

- `[1, 2] == [1, 2]` retourne `True`;
- `[1, 2] == [2, 1]` retourne `False`;
- `[1, 2] != [1, 2]` retourne `False`;
- `[1, 2] != [1, 2, 3]` retourne `True`;
- `3 in [1, 2, 3, 4]` retourne `True`;
- `3 in [1, 2, 5, 7]` retourne `False`.


## II Accéder aux éléments

Chaque élément est accessible par son **indice**. Les indices commencent à 0. Les éléments d'une liste `L` sont donc numérotés de 0 à `len(L)`-1.

!!! example "Exemple"
    ```python
    >>> L = [3,4,5,6]
    >>> L[3]
    6
    >>> L[0]
    3
    ```

On peut aussi accéder aux éléments en partant de la fin: -1 pour le dernier, -2 pour l'avant-dernier, etc.

!!! example "Exemple"
    ```python
    >>> L = [3,4,5,6]
    >>> L[-1]
    6
    >>> L[-3]
    4
    ```

On peut obtenir un extrait de ce tableau en désignant une  tranche (*slice* en anglais) . Pour cela il faut indiquer le premier élément et le dernier qui ne sera pas inclus séparés par `:`

!!! example "Exemple"

    ```python
    >>> L = [3,4,5,6,7,8]
    >>> L[1:4] # indices de 1 à 3
    [4,5,6]
    ```

**Exemple de lien entre indice et élément**

Dans l'exemple on utilise la liste définie par  
!!! example ""

    ```python
    >>> L = ['t', 'e', 's', 't']
    ```


C'est une liste de taille 4.

| Liste          | 't'  | 'e'  | 's'  | 't'  |
| -------------- | :--: | :--: | :--: | :--: |
| Indice         |  0   |  1   |  2   |  3   |
| Indice négatif |  -4  |  -3  |  -2  |  -1  |

On a donc:
!!! example ""

    ```python
    >>> L[2]
    's'
    >>> L[-3]
    'e'
    >>> L[4] # L'indice 4 sort des indices possibles
    IndexError: list index out of range
    ```

!!! warning "Attention"

    Tenter d'acceder à un indice qui n'existe pas engendre une erreur `IndexError: list index out of range`. Dans les boucles, il faut être vigilant afin de ne pas lever cette erreur.




Un tableau est *mutable*, ce qui veut dire qu'un fois créé il est possible de le modifier. 

### Modifier un élément d'un tableau

Pour modifier un élément par son indice, il suffit d'affecter une nouvelle valeur à cet élément.

!!! example "Exemple"

    ```python
    >>> L = [4, 5, 6]
    >>> L 
    [4, 5, 6]
    >>> L[1] = 7		 # L'élément d'indice 1 est affecté de la valeur 7
    >>> L
    [4, 7, 6]           # L'élément 5 d'indice 5 a été remplacé par 7
    >>>  L[0] = L[0] + 5 # L'élément d'indice 0 est affecté de la valeur L[0] + 5 = 4 + 5 =  9
    >>> L 
    [9, 7, 6]
    ```
## III Modifier un tableau - quelques méthodes
Une méthode est une fonction particulière qui agit directement sur  le tableau. Chaque modification se fait *en place* ce qui veut dire que le tableau est modifié et que l'on perd le tableau initial. 

### Ajouter d'un élément à la fin du  tableau

On ajoute un élément à la fin du tableau avec la méthode `append`:

!!! example "Exemple"

    ```python
    >>> L = [1,2,3]
    >>> L.append(4) # Ajoute l'élément 4 à la fin de la liste L
    >>> L
    [1, 2, 3, 4]
    ```

### Supprimer un élément

#### Connaissant son indice

On peut  supprimer un élément en utilisant son indice avec la méthode  `pop`.
`L.pop(i)` supprime l'élément d'indice `i` de `L`. Si l'indice `i` est omis, c'est le dernier élément de la liste qui est supprimé et renvoyé. 

!!! example "Exemple"

    ```python
    >>> L = [0, 1, 2, 3, 4, 5]
    >>> a = L.pop(2) # supprime l'élément d'indice 2 et retourne sa valeur
    >>> L
    [0, 1, 3, 4, 5]
    >>> a 
    2
    >>> L.pop() # supprime le dernier élément et le retourne.
    >>> L
    [0, 1, 3, 4 ]
    ```

### Connaissant sa valeur 
 `L.remove(elem)` permet de supprimer de `L` le premier élément valant `elem`. Si cet élément n'existe pas, une erreur `ValueError: list.remove(x): x not in list` est renvoyée. 

!!! example "Exemple"

    ```python
    >>> L = [1, 2, 3, 4, 3, 2]
    >>> L.remove(2)
    >>> L
    [1, 3, 4, 3, 2]
    >>> L.remove(5)
    ValueError: list.remove(x): x not in list
    ```

### Insérer un élément
`L.insert(i, elem )` permet d'insérer `elem` à l'indice `i` de la liste `L`. Si l'indice `i` est plus grand que le dernier indice de `L`, l'élément `elem` a ajouté à la fin de la liste `L`.

Si `L = [1, 2, 3, 4]` alors `L.insert(1, 7)` modifie la liste `L` en la liste `[1, 7, 2, 3, 4]`.

### Retrouver l'indice d'un élément
`L.index(elem)` retourne l'indice de la  première occurence de `elem` de `L` si il est présent dans `L` et une erreur sinon.

Si `L = [1, 2, 3, 4]` alors `L.index(2)` retourne `1`. `L.index(10)` retourne `ValueError: 10 is not in list`



## Opérateurs et tableaux
Les opérateurs `+` et `*` sont disponibles pour les tableaux. 

### Concaténer des tableaux

!!! example "Exemple"

    ```python
    L1 = [1,1,2,3]
    L2 = [5,8,13,21]
    L3 = L1  + L2
    print(L3)
    ```

    Ce programme affiche `[1, 1, 2, 3, 5, 8, 13, 21]`

!!! warning "Attention"

    Cette opération est couteuse car elle recopie  le contenu des tableaux `L1` et `L2` dans un nouveau tableau. Utiliser cette méthode à la place de `append` est donc souvent une assez mauvaise idée ! 

### Dupliquer un tableau

 `[O] * 3` retourne le tableau `[0, 0, 0]` qui est le tableau `[0]` dupliqué trois fois. 


## IV Parcourir un tableau

Un tableau est **itérable** cela veut dire qu'on peut le parcourir avec une boucle `for`.

Il y a deux façons de parcourir un tableau: par indice ou par élément.

### Parcours par indice

Pour parcourir par indice un tableau `T`, comme `#!python len(T)` renvoie la longueur du tableau `T`,  `#!python range(len(T))`  va renvoyer les nombres de `0` à `len(T) - 1`  c'est-à-dire tous les indices du tableau `T` 

Utiliser les indices permet de modifier les éléments durant le parcours.

!!! example "Exemple" 

    === "Exemple 1"

        ```python
        L = [2,4,6]
        for i in range(len(L)): # i est affecté des valeurs de 0 à 2
            print(L[i], end='') # Affiche L[i] sans aller à la ligne
        ```

        Affiche: `2 4 6`

    === "Exemple 2"

        ```python
        L = [2,5,6]
        for i in range(len(L)):
            L[i] = L[i] + 1
        print(L)
        ```

        Ajoute 1 à chaque éléments de la liste. La liste obtenue est alors:

        ```python
        [3,6,7]
        ```

### Parcours par élément

Les éléments sont lus un par un. Dans ce cas, l'indice des éléments n'est pas connu et il n'est pas possible de changer la valeurs des éléments.

!!! example "Exemple"

    === "Exemple 1" 

        ```python
        L = [2,4,6]
        for elt in L:			# elt est affecté des valeurs 2, 4 puis 6
            print(elt, end='')	# affiche elt sans aller à la ligne
        ```

        Affiche: `2 4 6`

    === "Exemple 2"

        ```python
        L = [2,5,6]
        for elt in L:
            if elt % 2 == 0:
                print(elt, end='')
        ```

        N'affiche  que les éléments pairs du tableau, soit:

        ```python
        2 6
        ```



## V Copier un tableau

Une variable à laquelle on affecte un tableau est en fait une *référence* à ce tableau.  En affectant un tableau à une variable,  on affecte en fait la référence de ce tableau. Les deux variables font alors référence au même tableau. Cela entraine que modifier un tableau modifie aussi l'autre.

!!! example "Exemple"

    === "Exemple"

        ```python
        L = [1,2,3]
        M = L
        M[0] = 4
        print(L)
        ```

        Affiche: `[4,2,3]` 

        En modifiant le tableau `M` on a aussi modifié le tableau `L` car en mémoire elle font référence au même tableau. 

    === "Voir dans PythonTutor"
        <iframe width="800" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=L%20%3D%20%5B1,2,3%5D%0AM%20%3D%20L%0AM%5B0%5D%20%3D%204%0Aprint%28L%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>





Afin d'éviter ce problème, il faut faire une copie de la liste à l'aide de la fonction `copy` du module `copy`.

!!! example "Exemple"

    === "Exemple"

        ```python
        import copy
        L = [1,2,3]
        M = copy.copy(L) #Recopie la liste
        M[0] = 4
        print(L)
        ```
        Affiche: `[1,2,3]` 

    === "Voir dans PythonTutor"

        <iframe width="800" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=import%20copy%0AL%20%3D%20%5B1,2,3%5D%0AM%20%3D%20copy.copy%28L%29%20%23Recopie%20la%20liste%0AM%5B0%5D%20%3D%204%0Aprint%28L%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>




!!! remark "Remarque"

    Pour des tableaux plus complexes, comme les tableaux de tableaux, il faut utiliser la fonction `deepcopy` du module  `copy` .

    ```python
    import copy
    L = [[1,2],[3,4]]
    M = copy.deepcopy(L)
    M[1] = [5,6]
    print(L)
    print(M)
    ```

    Affiche: 

    ```python
    [[1, 2], [3, 4]]
    [[1, 2], [5, 6]]
    ```

Pour aller plus loin: 

[https://docs.python.org/fr/3/tutorial/datastructures.html](https://docs.python.org/fr/3/tutorial/datastructures.html) 

