# Structures imbriquées

Il est possible de combiner listes, tuples et dictionnaires. Cela est utile par exemple pour représenter le plateau un jeu  de cartes, le plateau  d'un d’échec, un carnet d'adresse.

## Construction d'une structure imbriquée.
On peut imbriquer les structures comme l'on veut.  La seule restriction est que les clés d'un dictionnaire ne peuvent pas être un type mutable. Pour nous cela veut dire que les clés ne peuvent pas être des tableaux.

!!! example "Exemples"

    === "Exemple 1"

        `T` est un tableau de tuples. Il représente un main de quatre cartes d'un jeu de cartes. Il contient quatre tuples.  Chaque tuple comporte deux éléments. Le premier est la couleur et le second la valeur de la carte.

        ```python
        >>> T = [('pique',7),  ('trèfle',1),  ('coeur',9), ('pique',8)]
        >>> len(T) # Nombre d'éléments du tableau
        4
        >>> T[0]
        ('pique',7)
        >>> len(T[0]) # Nombre d'élément de l'élément d'indice 0 du tableau
        2
        ```
        Chaque élément du tableau est un tuple. 

        ```python 
        >>> type(T[0])
        <class 'tuple'>
        ```

        On peut accéder directement à la couleur ou à la valeur de la carte en utilisant deux fois la notation crochet.

        ```python
        >>> T[0][0]
        'pique'
        >>> T[0][1]
        7
        ```
    === "Exemple 2"

        `D`  est un dictionnaire qui représente une fiche d'un contact. Il contient trois clés `nom`, `prenom` et `telephones`. Aux deux premières clés sont associées des chaines de caractères représentants le nom et le prénom. La dernière clé est associée à un dictionnaire qui contient deux clés `mobile`  et `bureau`.
        
        Pour plus de lisibilité, on peut aller à la ligne après une virgule. 
        ```python
        >>> D = {'nom': 'Dupond', 'prenom': 'Jean',
                'telephones': {'mobile': '0612345678', 'bureau': '0187654321'}}
        >>> len(D) # Nombre de clé du dictionnaire
        3
        >>>  type (D)
        <class 'dict'>
        >>> D['nom']
        'Dupond'
        >>> D['telephones']
        {'mobile': '0612345678', 'bureau': '0187654321'}
        >>> type(D['telephones'])
        <class 'dict'>
        >>>  D['telephones']['mobile']
        '0612345678'
        ```
    === "Exemple 3"

        `T` représente la grille d'un jeu de dimensions 2 lignes et 3 colonnes.
        ```python
        >>> T = [[1, 2,  3],
                 [4, 5, 6]]
        >>> len(T) # Retourne le nombre  de lignes
        2
        >>> len(T[0]) # Retourne le nombre de colonnes car les deux lignes ont le même nombre de colonnes.
        3
        >>> T[1] # Retourne la ligne d'indice 1
        [4, 5, 6]
        >>>T[1][2] # Retourne l'élément de la ligne  d'indice 1 et de la colonne d'indice 2.
        6
        ```
        De façon plus générale  `T[i][j]` retourne l'élément de la ligne d'indice `i` et de la colonne d'indice `j` si ces indices existent.

        Il n'y a pas de moyen simple d'obtenir une colonne. Il est possible de la faire avec une boucle ou mieux avec une compréhension.

        Par compréhension:

        ```python
        >>> C = [ L[1] for L in T)] # Retourne la colonne d'indice 1 de T.
        >>> C
        [2, 5]
        ```

        Par une boucle:
        ```python
        C = []
        for L in T:
            C.append(L[1])
        print(C)
        ```
        Affiche `[2, 5]` 


## Modifier une structure imbriquée
La méthode est la même que pour les différents types construits étudiés.

!!! example "Exemples"

    === "Exemple 1"

        ```python
        >>> T = [[1,2,3],
            [4,5,6],
            [7,8,9]]
        >>> T[2][2] = 10 # Modification de l'élément de ligne d'indice 2 et de colonne d'indice 2
        >>> T
        [[1, 2, 3], [4, 5, 6], [7, 8, 10]]
        >>> T[1] = [11,12,13] # Modification de la ligne d'indice 1
        >>> T
        [[1, 2, 3], [11, 12, 13], [7, 8, 10]]
        ```

    === "Exemple 2"

        ```python
        >>> D = {'nom': 'Dupond', 'prenom': 'Jean',
                    'telephones': {'mobile': '0612345678', 'bureau': '0187654321'}}
        >>> D['telephones']['mobiles'] = '0711223344' # Remplace le numéro de téléphone mobile par '0711223344'
        >>> D
        {'nom': 'Dupond', 'prenom': 'Jean', 'telephones': {'mobile': '0711223344', 'bureau': '0187654321'}}
        >>> D['telephones'] =  {'mobile': '0788776655', 'bureau': '0455443322', 'fixe': '0412123434'  }
        >>> D
        {'nom': 'Dupond', 'prenom': 'Jean', 'telephones': {'mobile': '0788776655', 'bureau': '0455443322', 'fixe': '0412123434'  }}
        ```



## Parcourir une structure imbriquée
 
Pour parcourir une structure imbriquée, il faut soit utiliser deux boucles soit utiliser des compréhensions.

!!! example "Exemples"

    === "Exemple 1"

        `persos` est un tableau de dictionnaires. 

        ```python
        persos = [{"prenom": "Bilbo", "nom": "Baggins", "age": 111},
            {"prenom": "Frodo", "nom": "Baggins", "age": 33},
            {"prenom": "Sam", "nom": "Gamgee", "age": 21}]

        # Parcours du tableau
        for p in persos:
            print("--------")
            # Parcours d'un dict
            for k, v in p.items():
                print(k, '->', v)
        ```
        Affiche:

        ```python
        --------
        prenom -> Bilbo
        nom -> Baggins
        age -> 111
        --------
        prenom -> Frodo
        nom -> Baggins
        age -> 33
        --------
        prenom -> Gamgee
        nom -> Sam
        age -> 21
        ```

    === "Exemple 2"

        La fonction `maxi` prend en argument un tableau de tableaux qui contient des entiers et retourne la valeur du plus grand élément de ce tableau.

        ```python
        def maxi(T: list) -> int:
            maximum = None
            for ligne in T:
                for elem in ligne:
                    if maximum is None or maximum <  elem:
                        maximum = elem
            return elem
        Tab = [1, 3, 4], [5, 2, 7], [1, 1, 3]]
        print(maxi(Tab))
        ```
        Ce programme affiche `7`.   


## Copier un structure imbriquée 

L'affectation d'une structure imbriquée à une variable créer un alias  de celle-ci. Pour obtenir une structure  imbriquée indépendante de son originale, il faut utiliser la méthode `deepcopy` du module `copy` .

!!! example "Exemple"

    
    ```python
    >>> import copy as c
    >>> L1 = [[1,2,3],[4,5,6]]
    >>> L2 = c.deepcopy(L1)
    >>> L1[0][1] = 7
    >>> L2
    [[1, 2, 3], [4, 5, 6]]
    ```

    `L1` a bien été modifié sans que `L2` le soit.




