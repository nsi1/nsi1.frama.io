# Site pour les élèves de première NSI

Vous trouverez sur ce site des documents pour l'enseignement de la spécialité NSI en première.


### Basthon
Basthon pour Bac À Sable pour pyTHON, est un site qui permet d'utiliser en ligne un IDE ou des notebooks  dans un navigateur, sans installation.

- [Basthon](https://basthon.fr/) Permet d'utiliser une console, ou des notebooks Jupyter en ligne sans installation. 
  Il existe une version desktop qui permet d'en installer une version sur une clé USB.
- [Présentation des notebook Basthon](https://www.carnets.info/jupyter/debuter_jupyter/)
- [Notebook Basthon](/basthon-notebook) sur ce site.
- [Console Basthon](/basthon-console/) sur ce site.



###  Thonny et notebook Jupyter

* [Thonny](https://thonny.org/) 
* [Présentation de Thonny](https://patth.frama.io/nsi1ere/ressources/thonny/tour_thonny/)

* [Edupyter](https://www.edupyter.net/) Application portable contenant Thonny et Jupyter. (Pour windows uniquement).

* [Jupyter Lab](https://github.com/jupyterlab/jupyterlab-desktop/releases) (Linux, MacOs et Windows) Application permettant d'avoir Jupyter lab sans installer de serveur web.

  

