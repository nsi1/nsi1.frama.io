# Portes logiques

* [Cours portes logiques](/architecture/portes_logiques.pdf)
* [TD portes logiques](/architecture/TD-portesLogiques.pdf)

### Pour aller plus loin:

* [Nand Game](https://nandgame.com/) Du transistor à l'ordinateur. Jeu de puzzle en anglais.
* [De la porte Nand à l'assembleur (jeu payant sur Steam)](https://store.steampowered.com/app/1444480/Turing_Complete/) 
* [https://www.nand2tetris.org/](https://www.nand2tetris.org/) Cours  "From NAND 2 Tetris" auquel les deux jeux précédents rendent hommage.
* Le jeu d'arcade Atari Pong est un des premier jeu  électronique qui a eu beaucoup de succès au début des années 70. Il est constitué essentiellement de portes logiques et d'horloges reliées ensemble. Il est un précurseur des jeux utilisant des processeurs et des logiciels.
  * [Les circuits du jeu d'arcade Atari Pong](http://www.pong-story.com/LAWN_TENNIS.pdf)
  * [Simulation des circuits du jeu d'arcade Atari Pong](https://www.falstad.com/pong/)

