---
subtitle: "Architecture"
tags: [Architecture Von Neumann, assembleur]
---

# Architecture de Von Neumann

* [Cours architecture de Von Neumann](/architecture/ArchiVVonNeumann.pdf)
* [Simulateur assembleur AQA](https://csreith.com/cpu.php)
* [Simulateur compilation - assembleur](http://goupill.fr/up/)
* [TD assembleur](/architecture/M99-élèves.pdf) 
* [Lien vers le M99](https://raw.githubusercontent.com/InfoSansOrdi/M999/master/M99-memoire.pdf)
* [Vidéo: Histoire de l'architecture des ordinateurs](https://www.lumni.fr/video/une-histoire-de-l-architecture-des-ordinateurs)
* ["In 1978, the Cray 1 supercomputer cost $7 Million, weighed 10,500 pounds and had a 115 kilowatt power supply. It was, by far, the fastest computer in the world. (In 2013) The Raspberry Pi costs around $70 (CPU board, case, power supply, SD card), weighs a few ounces, uses a 5 watt power supply and is more than 4.5 times faster than the Cray 1" ](http://www.roylongbottom.org.uk/Cray%201%20Supercomputer%20Performance%20Comparisons%20With%20Home%20Computers%20Phones%20and%20Tablets.htm#anchor22)









