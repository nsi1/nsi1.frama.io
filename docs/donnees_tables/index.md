---
subtitle: "Données en tables"
tags: [CSV, data ]
---

# Données en tables

Dans de nombreux cas, un programme doit traiter de nombreuses données. Ces données peuvent être publiques en open data ou privées. Elles sont stockées dans différents formats de fichiers comme les formats CSV et JSON ou dans un base de données. Cette partie s'occupe de faire des traitements sur des fichiers au format CSV.

## Fichiers CSV et traitement de données sur les gares
   * [Attente en gare](DonnéesStructuréesCVS.pdf)
   * [Données sur les gares](gares-pianos.csv)
## Traitement du fichier provenant de l'INSEE sur les prénoms
   * [TP sur les prénoms](TraitementDonnées1Prénoms.pdf)
   * [Données INSE sur les prénoms](nat2020.csv)
## Un exemple de  fusion de tables
   * [TP sur la fusion de tables](TraitementDonnées2fusions2.pdf)
   * [Fichiers à télécharger](fichiers.zip)
   * [TP à faire en ligne sur Capytale avec le code 597a-16296](https://capytale.fr)
   * [TD tables](TDtables.pdf)

