---
subtitle: "Binaire, entiers, entiers signés, flottants, caractères"
tags: [entier, entiers signés, flottants, utf8, unicode]
---

# Types et valeurs de base

Dans un ordinateur tout est représenté par des 0 et des 1. Cette partie présente comment sont codés les type de base en binaire.



## Les entiers

* [Cours entiers](/binaire/Cours-entiersPositifs.pdf)
* [TP nombres entiers](/binaire/TD_nombres_entiers.pdf)
* [Site permettant de visualiser les entiers](https://integer.exposed) 
* [Jeu : binary game](https://learningcontent.cisco.com/games/binary/index.html)

## Les entiers signés

* [Cours entiers signés](/binaire/Cours-entiersSignés.pdf) 
* [TP entiers signés](/binaire/TD_nombres_entiers_signes.pdf)

## Les flottants

* [Cours flottants](/binaire/Cours-flottants.pdf)
* [TP flottants](/binaire/TP_flottants.pdf)
* [Site permettant de visualiser les flottants](https://float.exposed)

##  Le codage des caractères

* [Cours codage des caractères](/binaire/Cours-codageCaractères.pdf) 
* [Tableau ASCII](http://vivienfrederic.free.fr/ascii.pdf)
* [TP codage de caractères](/binaire/TP_caracteres.pdf)
* ??? important "Exemple justifiant la convention sur les noms de  variables"

        Par convention il est préconisé de n'utiliser que les caractères de la table ASCII dans les noms de variables et de fonctions.

        ===  "Exemple"

            Faites fonctionner ce programme.
            {{ IDE("binaire/exemple_noms_var")}}

        ===  "Explication"

            La fonction `name` du module `unicodedata` prend en paramètre un caractère et retourne une chaine de caractères décrivant ce caractère. 
            
            Faites fonctionner ce programme.
            {{ IDE("binaire/exemple_noms_var_explications") }}

            Vous devez constater que ce programme contient deux noms de variables différentes qui se ressemblent tellement qu'on ne peut pas les distinguer.

            

### Pour en savoir plus:

- [Document sur des erreurs dépassement de capacités](/binaire/dépassementCapacité-Arrondis.pdf)
- [Page montrant des exemples concrets d'erreurs liées aux flottants (en)](https://jvns.ca/blog/2023/01/13/examples-of-floating-point-problems/)
- [Des exemples  de ce que l'on peut coder sur  8 octets](https://cdn.masto.host/socialjvnsca/media_attachments/files/109/825/084/829/751/365/original/50131c775feb02c9.png)
- [Site permettant de décomposer les  caractères écrit en unicode](https://www.fontspace.com/unicode/analyzer#e=8J-RqOKAjfCfkanigI3wn5Gn4oCN8J-Rpi4)



