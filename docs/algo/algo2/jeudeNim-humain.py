#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 21 10:32:50 2022

@author: druet
"""
import random
import time


################### le message de bienvenue et d'explication ##################

def welcome_message():
    print("""
----------------------------------------------------------------------------
                        Règles du jeu de Nim
----------------------------------------------------------------------------
Règles : 1 - L'utilisateur et l'ordinateur jouent à tour de rôle.
         2 - L'ordinateur commence.
         3 - Seulement 1 ou 2 bâtons peuvent être retirés à chaque coup.
         4 - Celui qui tire le dernier bâton a gagné.
         
----------------------------------------------------------------------------
                           Apprentissage
----------------------------------------------------------------------------                        
Suivant qu'il gagne ou perd, l'ordinateur reçoit une récompense ou une punition.
----------------------------------------------------------------------------
""")

#### Nous faisons commencer l'ordinateur parce que le premier joueur a une 
#### stratégie gagnante. Si le joueur commence et qu'il joue bien, la machine 
#### perdra quoiqu'elle fasse et n'apprendra rien. 

###############################################################################


################## définition et impression de la position de jeu #############

def printboard(n):
    board=[]
    for _ in range(n):
        board.append("/")
    print("\n----------------------------------------------------------------------------")
    print("      ",*board, sep="   ")
    print("----------------------------------------------------------------------------\n")
    print("Il reste " + str(n) + " allumettes.")
    
###############################################################################


################### initialisation de la machine ##############################

nombre_allumettes=8 ### vous pouvez changer le nombre d'allumettes de départ
board = [] ### le plateau de jeu
boulesjaunes = [] ### correspond au nombre de boules jaunes dans la case
boulesrouges = [] ### correspond au nombre de boules rouges dans la case
tirage = [] ### correspondra au tirage dans une partie
# Initialise avec 2 boules rouges et 2 boules jaunes dans chaque case.
for _ in range(nombre_allumettes):
    boulesjaunes.append(2)
    boulesrouges.append(2)
    tirage.append(0)
#### Attention Python commence à 0 #####
boulesrouges[0]=0 #### il ne faut pas mettre de boules rouges dans la case 0, coup interdit


###############################################################################


#################### programme principal : jeu + renforcement #################
welcome_message()
uneautrepartie=True
compteur_partie=0
time.sleep(1)
print("La probabilité de gain de l'ordinateur à la première partie si le joueur joue optimalement est de 12,5 %")
time.sleep(1)
while uneautrepartie :
    player = "CMP"
    allumettes=nombre_allumettes
    tirage=[0,0,0,0,0,0,0,0]
    while allumettes>0:
        printboard(allumettes)  ### imprime la position de jeu
        time.sleep(1)
        if player=='CMP': ### c'est à l'ordinateur de jouer
            print("\nL'ordinateur choisit de retirer...")
            time.sleep(1)
            somme=boulesjaunes[allumettes-1]+boulesrouges[allumettes-1]
            boulehasard=random.randint(1,somme)##permet de tirer jaune ou rouge
            if boulehasard <= boulesjaunes[allumettes-1]: ##la machine a tiré jaune, i.e. elle enlève une allumette
                tirage[allumettes-1]=1
                allumettes=allumettes-1
                print("1 allumette.")
            else:
                tirage[allumettes-1]=2
                allumettes=allumettes-2
                print("2 allumettes.")
            if allumettes==0:
                winner='CMP'
            else:
                player='USER'  
        else: ### c'est au joueur de jouer
#### on demande au joueur ce qu'il souhaite jouer avec vérification que c'est un coup légal
            coup_joueur=0
            print("\n--À vous de jouer !--")
            while coup_joueur not in range(1, 3) or coup_joueur>allumettes:
                try:
                    coup_joueur = int(input("\nQuel est votre choix ?"))
                    if coup_joueur == 0:
                        print("\nVous devez enlever au moins une allumette !")
                    elif coup_joueur not in range(1, 3) or coup_joueur>allumettes:
                        print("\nVous ne pouvez pas enlever autant d'allumettes !")
                        coup_joueur = int(input("\nQuel est votre choix ?"))
                except Exception as e:
                    print("\nCela ne semble pas une réponse valide.\nError: " + str(e) + "\nRecommencez !")
##### fin du choix du joueur
            allumettes=allumettes-coup_joueur
            if allumettes==0:
                winner='USER'
            else:
                player='CMP' 
    compteur_partie+=1
##### fin de la partie ######
##### annonce des résultats #####
    if winner=='CMP':
        print("\n----------------------------------------------------------------------------")
        print("L'ordinateur a gagné, nous allons le récompenser.")
        print("----------------------------------------------------------------------------\n")
    else:
        print("\n----------------------------------------------------------------------------")
        print("Bravo ! Vous avez gagné ! Nous allons punir l'ordinateur.")
        print("----------------------------------------------------------------------------\n")
##### Apprentissage : récompense ou punition de l'ordinateur#####        
    if winner=='CMP': ###récompense
        for i in range(nombre_allumettes):
            if tirage[i]==1:
                boulesjaunes[i]=boulesjaunes[i]+1
            if tirage[i]==2:
                boulesrouges[i]=boulesrouges[i]+1
    else: ###punition
        for i in range(nombre_allumettes):
            if tirage[i]==1:
                boulesjaunes[i]=boulesjaunes[i]-1
            if tirage[i]==2:
                boulesrouges[i]=boulesrouges[i]-1
####### fin de la récompense ou de la punition #################
####### réinitialisation des verres vides ######################
    for i in range(nombre_allumettes):
        if (boulesjaunes[i]==0) and (boulesrouges[i]==0):
            boulesjaunes[i]=2
            boulesrouges[i]=2
######## impression de l'état des verres #######################
    time.sleep(1)
    for i in range(nombre_allumettes):
        print("Dans le verre " + str(i+1) +", il y a " + str(boulesjaunes[i]) + " boules jaunes et " + str(boulesrouges[i]) + " boules rouges.")
################################################################
############ calcul de la probabilité de gagner ################
    time.sleep(1)
    if (boulesjaunes[3]/(boulesjaunes[3]+boulesrouges[3]))>(boulesrouges[4]/(boulesjaunes[4]+boulesrouges[4])):
        proba=(boulesrouges[7]/(boulesjaunes[7]+boulesrouges[7]))*(boulesrouges[4]/(boulesjaunes[4]+boulesrouges[4]))*(boulesrouges[1]/(boulesjaunes[1]+boulesrouges[1]))
    else:
        proba=(boulesrouges[7]/(boulesjaunes[7]+boulesrouges[7]))*(boulesjaunes[3]/(boulesjaunes[3]+boulesrouges[3]))*(boulesrouges[1]/(boulesjaunes[1]+boulesrouges[1]))
    print("\n----------------------------------------------------------------------------")
    print("Vous avez joué "+ str(compteur_partie) +" parties.")
    print("----------------------------------------------------------------------------\n")
    print("\n----------------------------------------------------------------------------")
    print("La probabilité de gain de l'ordinateur à la prochaine partie si le joueur joue optimalement (en connaissant l'état des verres :-)) est de " + str(round(proba*100,2)) + "%")
    print("----------------------------------------------------------------------------\n")
################################################################
############ On continue ? ################    
    test=True
    while test:
        another_go = input("\nVoulez-vous rejouer ?[O/N]: ")
        if another_go in ("o","O"):
            uneautrepartie=True
            test=False
        elif another_go in ("n","N"):
            uneautrepartie=False
            test=False
        else:
            print("\nChoix invalide. Recommencez !")    
############################################            
            




 


