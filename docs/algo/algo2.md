---
subtitle: "Algorithmes avancés"
tags: [algorithme, knn, k plus proches voisins, algorithmes gloutons]
---

# Algorithmes avancés

## $k$ plus proches voisins.
L'algorithme des $k$ plus proches voisins, *$k$-nearest neighbors* en anglais souvent abrégé en *knn* est un algorithme utilisé en intelligence artificielle.

Il permet par un apprentissage automatique  de prédire la classification d'un élément. 

* [Cours Knn (auteur Julien de Villèle)](algo2/algo2CoursKnn.pdf) 
  * [Image 1](algo2/algo_1_sur_2.gif)
  * [Image 2](algo2/algo_2_sur_2.gif) 
* [TD knn (auteur Julien de Villèle)](algo2/exercicesKnn.pdf)
* [Le fichier iris](algo2/iris.csv)
* [TP Knn le fichier Iris](https://capytale2.ac-paris.fr/web/c/e25f-24902) 

## Exemples d'algorithmes gloutons 
Un algorithme glouton, *greedy algorithm*  en anglais, est un algorithme d'optimisation qui cherche à trouver un minimum ou un maximum.

* [Le cours TD](algo2/algo2-glouton.pdf)
* [Le TP rendu de monnaie](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/8ee7-22419) 
* [Animation d'introduction au problème du sac à dos](http://hmalherbe.fr/thalesm/gestclasse/documents/Premiere_NSI/Animations/sac_a_dos/p5.js/sac_a_dos.html)
* [Le TP problème du sac à dos](https://capytale2.ac-paris.fr/web/c-auth/list?returnto=/web/code/a620-22416) 

## Jeu de Nim et apprentissage par renforcement

- [Activité débranchée jeu de Nim](algo2/ia-jeu-de-Nim.pdf) 
- [Apprentissage par renforcement du jeu de Nim en Python](/basthon-console/?from=/algo/algo2/jeudeNim-humain.py)
- Source [https://culturesciencesphysique.ens-lyon.fr/ressource/IA-Nim-1.xml](https://culturesciencesphysique.ens-lyon.fr/ressource/IA-Nim-1.xml)

## Pour en savoir plus sur l'intelligence artificielle

* "L'IA est créative mais non créatrice" (Auteur inconnu)
* [Vidéo: Démystifier l'intelligence artificielle](https://www.youtube.com/watch?v=B64xi7y9UxA) 
* [Vidéo: IA apprentissage automatique](https://www.youtube.com/watch?v=qO00I8vU81A)
* [Article: Qu’est-ce que l’intelligence artificielle ? Yann LeCun](https://www.college-de-france.fr/site/yann-lecun/Recherches-sur-l-intelligence-artificielle.htm) 
* [BD: Intelligences artificielles: Miroirs de nos vies](https://www.editions-delcourt.fr/bd/series/serie-intelligences-artificielles/album-intelligences-artificielles) 
* [Vidéo: comprendre le Machine Learning: L'algorithme du KNN](https://www.youtube.com/watch?v=9pvbEP1eyNY&feature=youtu.be) 
