---
subtitle: "Utilisation du clavier - raccourcis clavier"
tags: []
---



# Des caractères spéciaux utiles et quelques raccourcis

## Des caractères spéciaux

| Caractères | Touches             | Français              | Anglais         |
| ---------- | ------------------- | --------------------- | --------------- |
| \^         | \^+espace           | accent circonflexe    | hat             |
| _          | 8                   | tiret bas             | underscore      |
| &          | 1                   | éperluette            | commercial and  |
| ~          | AltGr+2 puis espace | tilde                 | tilde           |
| \#         | AltGr+3             | dièse                 | hash            |
| \|         | AltGr+6             | barre verticale       | pipe            |
| \          | AltGr+8             | barre oblique inverse | anti-slash      |
| @          | AltGr+0             | arobase               | (commercial) at |
| \{         | AltGr+4             | accolade ouvrante     | left brace      |
| \}         | AltGr+=             | accolade fermante     | right brace     |
| \[         | AltGr+5             | crochet ouvrant       | left bracket    |
| \]         | AltGr+)             | crochet fermant       | right bracket   |
| '          | 4                   | guillemet simple      | simple quote    |
| "          | 3                   | guillemet (double)    | double quote    |
| \`         | AltGr+7 puis espace | guillemet inversée    | backquote       |

## Quelques raccourcis

| Raccourci | Action                                  |
| --------- | --------------------------------------- |
| Ctrl+C    | Copie la partie sélectionnée \(copy\)   |
| Ctrl+V    | Colle la partie sélectionnée            |
| Ctrl+X    | Coupe la partie sélectionnée            |
| Ctrl+A    | Sélectionne tout \(all\)                |
| Ctrl+S    | Sauvegarde le travail en cours \(save\) |
| Ctrl+Z    | Annule la dernière modification         |