---
subtitle: "Sitographie"
tags: [NSI, Python]
---

# Sitographie

### Enseignements de la spécialité NSI

* <https://pixees.fr/informatiquelycee/n_site/nsi_prem.html>
* [http://monlyceenumerique.fr/nsi_premiere/index.html](http://monlyceenumerique.fr/nsi_premiere/index.html)
* <http://www.maths-info-lycee.fr/nsi_1ere.html>
* [https://nsi.xyz/](https://nsi.xyz/) 
* <https://thibautdeguillaume.fr/nsi_premiere.html>
* [https://www.carnets.info/](https://www.carnets.info/)
* [Correction des sujets d'E3C sur le site de David Roche](https://pixees.fr/informatiquelycee/prem/bns/)
* [NSI4NOOBS](http://nsi4noobs.fr/) 
* [https://nsi-snt.ac-normandie.fr/](https://nsi-snt.ac-normandie.fr/) 
* [https://ens-fr.gitlab.io/nsi1/](https://ens-fr.gitlab.io/nsi1/) 
* [https://journee-nsi.fr/](https://journee-nsi.fr/) 
* [https://e-nsi.gitlab.io/pratique/](https://e-nsi.gitlab.io/pratique/) Site d'entrainement à la pratique de la programmation
* [https://codex.forge.apps.education.fr/](https://codex.forge.apps.education.fr/) Site d'entrainement très gradué à la pratique de la programmation. 
* [https://pratique.forge.apps.education.fr/](https://pratique.forge.apps.education.fr/) Site d'entrainement
* [https://cours-nsi.forge.apps.education.fr/premiere/](https://cours-nsi.forge.apps.education.fr/premiere/) Site complet contenant l'ensemble du programme de première NSI issu du travail collaboratif de professeurs de NSI.
* [https://nsi1.frama.io/basthon-notebook/?from=/ressources/epreuves_pratiques_2023_exercices1.ipynb](https://nsi1.frama.io/basthon-notebook/?from=/ressources/epreuves_pratiques_2023_exercices1.ipynb) NoteBook contenant les exercices 1 de l'épreuve pratique 2023

### Python

* <https://docs.python.org/3/index.html>
* <https://fr.wikibooks.org/wiki/Programmation_Python>
* [Livre de Gérard Swinnen sur Python 3](https://inforef.be/swi/download/apprendre_python3_5.pdf) (licence creative commmons)
* Documentation TkInter : <http://tkinter.fdex.eu/index.html> 
* Apprentissage en ligne:
    - [Silent teacher Python ](https://silentteacher.toxicode.fr/?theme=basic_python)
    - [Sciency](https://sciency.co/)
    - [Base Python pour le lycée](https://tech.io/playgrounds/56931/les-bases-de-python-pour-le-lycee/presentation)
    - [Future coder introduction interactive à Python](https://fr.futurecoder.io/course/#toc)
    - [https://snt-nsi.fr/python/](https://snt-nsi.fr/python/) 
    - [France IOI](fhttp://www.france-ioi.org/algo/chapters.php) Parcours gradué qui va jusqu'au niveau terminal.
    - [CodinGame](https://www.codingame.com) Vite difficile.
* Apprentissage par le jeu
  
    - [Py-rates](https://py-rates.fr/)  Du Python facile.
    - [RoboZZle](http://www.robozzle.com/beta/index.html?puzzle=-1)  Vous devez récupérer les étoiles. Des niveaux faciles mais certains vous ferons réfléchir.
    - [CargoBot](http://www-verimag.imag.fr/~wack/CargoBot/) Vous devez programmer la grue pour obtenir le motif du dessus.
    - [CubeComposer](https://david-peter.de/cube-composer/) Appliquer des fonctions pour transformer un mur  colorés en un autre. Quelques niveaux faciles, assez vite difficile.
* - [TP Les bases de Python](http://prof.math.free.fr/nsi/premiere/python1.html#haut)  sur le site [prof.math.free.fr](http://prof.math.free.fr/nsi/premiere/python1.html#haut)
    - [TP les bases de Python](https://progalgo.fr/) sur le site https://progalgo.fr/

### HTML CSS

* HTML:
    * Documentation <http://w3schools.com/tags/> 
    * Validateur en ligne : <https://validator.w3.org/#validate_by_upload>
* CSS: 
    * Documentation <http://www.w3schools.com/css/>
    * Validateur en ligne : <https://validator.w3.org/unicorn/?ucn_lang=fr#validate-by-upload+task_conformance>
  * [Cours HTML-CSS-javascript](https://publish.obsidian.md/yannhoury/Documentation/HTML-CSS/Cours+HTML+CSS+JS/Index) Très bon cours d'initiation  de Yann Houry

## Sites institutionnels

* [Programme de première NSI](https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf)
* Sujet 0 de l'épreuve de contrôle continue de fin de première :
    * [Sur Eduscol](https://cache.media.eduscol.education.fr/file/Annales_zero_BAC_2021_1e/87/9/S0BAC21-1e-SPE-NSI_1133879.pdf)
    * [Sujet 0 qui avait été mis en ligne puis remplacé]( https://framagit.org/flaustriat/nsi-1/blob/master/docs/sujet00.pdf)
    * [Les sujets officiels d'E3C]( http://quandjepasselebac.education.fr/e3c/#BNS%2FBac%20G%C3%A9n%C3%A9ral%2FEnseignements%20de%20sp%C3%A9cialit%C3%A9%2FSp%C3%A9cialit%C3%A9%20num%C3%A9rique%20et%20sciences%20informatiques%2Fe3c-2)
* [Sujets de l'épreuve pratique de terminale](https://eduscol.education.fr/2661/banque-des-epreuves-pratiques-de-specialite-nsi)
* [Vidéos sur Lumni](https://www.lumni.fr/lycee/premiere/segment/numerique-et-sciences-informatique-en-premiere) 

## Aide-mémoire (cheat sheet)

* [Python ](https://perso.limsi.fr/pointal/_media/python:cours:mementopython3.pdf)
* [Annotations de type en python](https://mypy.readthedocs.io/en/stable/cheat_sheet_py3.html#type-hints-cheat-sheet)
* [Markdown](https://www.carnets.info/jupyter/memomd/)
* HTML / CSS : 
    * [Menento HTML CSS](http://cyrilaudras.fr/attachments/article/3/MementoHTML-CSS.pdf) 
    * [https://htmlcheatsheet.com/](https://htmlcheatsheet.com/) 
    * [https://caniuse.com/#home](https://caniuse.com/#home) 
* JavaScript: [https://htmlcheatsheet.com/js/](https://htmlcheatsheet.com/js/)
* [Bash](https://framagit.org/flaustriat/nsi-1/blob/master/menentoBash.pdf)
* [Aide-mémoire informatique anglais-français](https://www.gregoirenoyelle.com/wp-content/uploads/2010/03/Aide_memoire_informatique-Eng_fr.pdf)
* [Reconnaissance graphique de caractères unicode](https://shapecatcher.com/) 
* [Antisèche Git](https://gdevops.gitlab.io/tuto_git/_downloads/ff11bc9f353dcc59720470099fbcf5da/froggit_git_antiseche.pdf) 
* [Jointures SQL](/img/ressources/jointuresSQL.jpg)

## Après la NSI - orientation

* [https://www.lumni.fr/programme/dans-mon-job-les-metiers-du-numerique](https://www.lumni.fr/programme/dans-mon-job-les-metiers-du-numerique)
* [https://gitlab.com/eskool/profs-info/nsi-public/-/wikis/Orientation](https://gitlab.com/eskool/profs-info/nsi-public/-/wikis/Orientation) 
* [Article de l'étudiant sur la spécialité NSI](https://www.letudiant.fr/lycee/specialites-bac-general/article/la-specialite-nsi-en-un-clin-d-oeil.html) 
* [Onisep: Les métiers des mathématiques, de la statistique et de l'informatique](https://www.onisep.fr/Decouvrir-les-metiers/Des-metiers-qui-recrutent/La-collection-Zoom-sur-les-metiers/Les-metiers-des-mathematiques-de-la-statistique-et-de-l-informatique)
* [Pod-NSI un pocast sur l'orientation post-bac pour les élèves ayant suivi NSI](https://pixees.fr/podcast/pod-nsi-orientation-post-bac/)
* [Exemples de suite d'études](https://nsi.enseigne.ac-lyon.fr/spip/spip.php?article80)
* [Site d'étudiants de prépa MP2I](https://prepas-mp2i.github.io/)
* [Ingénieur ou technicien en informatique?](https://www.youtube.com/watch?v=hjUWU73rxvY)
* [BD un devOps c'est quoi?](https://blog.octo.com/le-devops-cest-quoi/)
* [Devenir devOps](https://www.youtube.com/watch?v=tiSfXCM8VTw)
* [Etude sur le marché de l'emploi dans le numérique faite par codingGame](https://www.codingame.com/work/fr/codingame-developer-survey-2021/) 



## BD

* [CommitStrip](https://www.commitstrip.com/fr/) Le blog qui raconte la vie des codeurs.
* [Les BD d'Aryana Pezé DevOps]( https://blog.octo.com/author/aryana-peze-arp/ ) 
* [https://xkcd.com/](https://xkcd.com/)
* [Les Zines de Julia Evans](https://twitter.com/b0rk)
* [Grisebouille de Gee](https://grisebouille.net/category/tu-sais-quoi/)
* [Dis c'est quoi (Gee)](https://debian-facile.org/projets/discestquoi/) 
* [Les décodeuses du numérique](https://ins2i.cnrs.fr/les-decodeuses-du-numerique) 




## Autre


* [https://interstices.info/](https://interstices.info/) Une revue de culture scientifique proposant des articles souvent accessibles sur les sciences du numérique.
* [https://www.lemonde.fr/blog/binaire/](https://www.lemonde.fr/blog/binaire/) Blog sur les sciences du numérique.
* [Cours du collège de France](https://www.college-de-france.fr/site/chaires-annuelles-historique/Chaire-Informatique-et-sciences-numeriques.htm)
* [Cnil et IA](https://www.cnil.fr/fr/intelligence-artificielle/la-cnil-publie-ressources-grand-public-professionnels) 
* [https://hist-math.fr/](https://hist-math.fr/) Podcasts sur la (pré-)histoire de l'informatique et des maths. 
* [https://gitlab.com/xavki/sommaire-xavki-tutos-fr](https://gitlab.com/xavki/sommaire-xavki-tutos-fr) Des tutos vidéos des débutants jusqu'aux devOps ou les ingénieurs en informatiques.
* [Des bugs ...](http://tisserant.org/cours/qualite-logiciel/qualite_logiciel.html) 
* [Visualisation d'algorithmes](https://visualgo.net/en) 
* [Nand2tretis ](https://www.nand2tetris.org/) et en ligne: [https://nandgame.com](https://nandgame.com) 
* [livre: Informatique et culture scientifique du numérique(pdf)](https://hal.inria.fr/hal-03346079/file/inria-mooc-web-2021-04-14.pdf)
* [De la porte Nand à l'assembleur (jeu payant sur Steam)](https://store.steampowered.com/app/1444480/Turing_Complete/) 
* [Human Resource  Machine](http://tomorrowcorporation.com/humanresourcemachine) jeu payant sur le fonctionnement d'un ordinateur suivant l'architecture de Harward. [Version "Hour Of Code"](https://tomorrowcorporation.com/human-resource-machine-hour-of-code-edition) 
* [While True: Learn()](https://www.gog.com/game/while_true_learn) Jeu payant de programmation pour ceux qui s'intéresse au machine learning

## Cybersécurité
- Site de ressources autour de la cybersécurité: [https://cybersecurite.forge.apps.education.fr/cyber/](https://cybersecurite.forge.apps.education.fr/cyber/)

- Sites de découverte de la cybersécurité par des épreuves de type Capture The Flag (CTF)
   - [root-me](https://www.root-me.org/)
   - [picoctf](https://picoctf.org/)

