
# Découverte du module Pyxel
Pyxel est un module Python qui permet de créer des jeux de type retro-gaming.

C'est-à-dire que par exemple le nombre de couleurs est limité à 16 et il ne peut y avoir plus de  quatre sons simultanement.

Il contient son propre éditeur d'images et de sons.

Enfin, les jeux créés peuvent être accessible depuis une page web. 

### Le site pyxelstudio.net

Le site [pyxelstudio.net](https://pyxelstudio.net) permet de créer un jeu en python à l'aide du module pyxel en ligne.

Pour conserver votre travail vous pouvez soit créer un compte, soit conserver l'URL de votre projet.

### Fonctionnement de l'interface


- Ouvrez un navigateur sur [pyxelstudio.net](https://pyxelstudio.net)
- Cliquez sur "New Project"
- Dans l'interface "copy & save" est le code et l'URL qui permettent de retrouver votre travail. Il faut les sauvegarder pour retrouver votre travail.
- "share this project" est l'URL qui permet de partager le jeu et d'y jouer sans que le code soit visible.
- "ressource file name: res.pyxres" indique que le fichier en .pyxres qui contient les images et les sons doit se nommer obligatoirement res.pyxres.
- "Project Name" est le nom de votre projet"
- "description & documentation" sont à remplir pour les joueurs. Cela permet de décrire le type de jeu et de documenter son fonctionnement. (touches, etc.)


### Exécuter un programme

- Cliquez sur le triangle vert dans la fenêtre de gauche;
- Puis sur "click here to enable keyboard/mouse " 


### Structure d'un jeu

La manière élégante de programmer serait de le faire en programmation objet qui est au programme de terminale. Dans le cadre du programme de première, nous allons être obligé d'utiliser des variables globales.

!!! example "Premier exemple"
    
    Ce programme va afficher un rectangle qui change de couleur à chaque seconde.
    
    Dans un programme pyxel il y aura toujours:
    - Une fonction `update` qui correspond aux mises à jour des éléments
    - Une fonction `draw` qui créer une des images du jeu.
    - Le programme se termine par `pyxel.run(update, draw)`. 


    ```python
    import pyxel
    # Créer une fenêtre de 160 pixels de largeur
    # sur 120 de haut
    # l'origine est en haut à gauche
    # par défaut l'image est actualisée 30 fois par seconde
    pyxel.init(160, 120)
    # Variable globale
    couleur = 8

    def update():
        # global oblogatoire pour affecté une
        # nouvelle valeur à une variaglle
        global couleur
        # pyxel.frame_count est une variable qui
        # contient le numero de l'image en cours
        # si ce nombre est un multiple de 30 soit 1s.
        # le rectangle change de couleur
        if pyxel.frame_count % 30 == 0:
            if couleur == 11:
                couleur = 8
            else:
                couleur = 11

    def draw():
        #efface l'écran
        pyxel.cls(0)
        # trace un rectangle dont le coin
        # supérieur gauche a pour coordonnées 10, 10
        # de côtes de taille 20 et 20
        # de couleur la valeur de la variable couleur
        pyxel.rect(10, 10, 20, 20, couleur)

    pyxel.run(update, draw)
    ```



## Un space invader



!!! example "Début d'un space invader"
    Voici le début d'un programme qui va permettre de construire un space invader.
    Pour l'instant, le vaisseau va être représenté par un carré de côté 8 pixels.


    ```python
    import pyxel

    # taille de la fenetre 128x128 pixels
    # ne pas modifier
    pyxel.init(128, 128)

    # position initiale du vaisseau
    # (origine des positions : coin haut gauche)
    vaisseau_x = 60
    vaisseau_y = 60

    def vaisseau_deplacement(x, y):
        """déplacement avec les touches de directions.
        Retourne le nouvelles coordonnées
        """

        if pyxel.btn(pyxel.KEY_RIGHT):
            x = x + 1
        if pyxel.btn(pyxel.KEY_LEFT):

            x = x - 1
        if pyxel.btn(pyxel.KEY_DOWN):
                y = y + 1
        if pyxel.btn(pyxel.KEY_UP):
                y = y - 1
        return x, y


    # =========================================================
    # == UPDATE
    # =========================================================
    def update():
        """mise à jour des variables (30 fois par seconde)"""

        global vaisseau_x, vaisseau_y

        # mise à jour de la position du vaisseau
        vaisseau_x, vaisseau_y = vaisseau_deplacement(vaisseau_x, vaisseau_y)


    # =========================================================
    # == DRAW
    # =========================================================
    def draw():
        """création des objets (30 fois par seconde)"""

        # vide la fenetre
        pyxel.cls(0)

        # vaisseau (carre 8x8)
        pyxel.rect(vaisseau_x, vaisseau_y, 8, 8,8 )

    pyxel.run(update, draw)
  
    ```

    
La suite du tutoriel se trouve ici:
[https://www.cahiernum.net/J682W5/](https://www.cahiernum.net/J682W5)

Le fichier de resources d'images ce trouve [ici](res.pyxres) Vous devez le telecharger puis le téléverser dans pyxel studio afin que cela fonctionne. 

Vous aurez besoin de méthodes sur les tableaux.


- La méthode `append` permet d'ajouter l'élément donner en argument à la fin d'un tableau.
    ```python
    >>> T = [1,2,3]
    >>> T.append(5) # ajoute l'entier 5 au tableau T
    >>> T
    [1, 2, 3, 5]
    ```
- La méthode `remove` supprime l'élément dont l'indice est donné en argument
    ```python
    >>> T = ['a','b', 'c', 'd']
    >>> T.remove(2) # Supprime l'élement d'indice 2 soit 'c'
    >>> T
    ['a', 'b', 'd']
    ```

## Documentation

- [Documentation de pyxel en Français](https://github.com/kitao/pyxel/blob/main/docs/README.fr.md)
- [Liste des touches](https://github.com/kitao/pyxel/blob/main/python/pyxel/__init__.pyi) ce sont les noms commençant par `KEY`.
- Pour installer Python et Pyxel sur une clé USB téléchargez [édupyter](https://www.edupyter.net/) 
- [Documentation synthétique Pyxel Nuit du c0de](https://forge.apps.education.fr/nuit-du-code/DOCUMENTATION/-/raw/main/docs/assets/documents/documentation-pyxel.pdf)
- [Tutoriel pour écrire un snake](https://nuitducode.github.io/DOCUMENTATION/PYTHON/TUTORIELS/autres-tutoriels/tuto1/Snake_Pyxel.pdf)
