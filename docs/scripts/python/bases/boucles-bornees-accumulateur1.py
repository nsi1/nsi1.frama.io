def score_lettre(car: str) -> int:
    if car == 'a':
        return 1
    else:
        return 3

def score_mot(ch: str) -> int:
    s0 = score_lettre(ch[0])          #<------------- 4 variables s0, s1, s2, s3
    s1 = score_lettre(ch[1])          #<-
    s2 = score_lettre(ch[2])          #<-
    s3 = score_lettre(ch[3])          #<-  
    score = s0 + s1 + s2 + s3         #<------------- qu'on additionne à la fin
    return score

s = score_mot('babb')
print(s)