def score_lettre(car: str) -> int:
    if car == 'a':
        return 1
    else:
        return 3

def score_mot(ch: str) -> int:
    score = 0
    score = score + score_lettre(ch[0])          #<------------- 1 seule variable score
    score = score + score_lettre(ch[1])          #<-             qui accumule les points 
    score = score + score_lettre(ch[2])          #<-             au fur et à mesure
    score = score + score_lettre(ch[3])          #<- 
    return score

s = score_mot('babb')
print(s)