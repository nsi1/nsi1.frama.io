def score_lettre(car: str) -> int:
    if car == 'a':
        return 1
    else:
        return 3

def score_mot(ch: str) -> int:
    score = 0
    for i in range(0, 4):
        score = score + score_lettre(ch[i])
    return score

s = score_mot('babb')
print(s)