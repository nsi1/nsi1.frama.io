def table(nombre: int):
    """ 
    Cette procédure affiche la table de multiplication
    du paramètre nombre qui est un entier.
    """
    for i in range(11):
        # i prend successivement les valeurs de 0 à 10
        print(i,"fois", nombre, "égal", i * nombre)