from turtle import *

def carre():
    for i in range(4):  # Répète 4 fois:
        forward(100)    # Avance de 100 pixels
        left(90)        # Tourne de 90 degrés vers la gauche

carre()
