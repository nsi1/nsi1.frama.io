def score_lettre(car: str) -> int:
    if car == 'a':
        return 1
    else:
        return 3


def score_mot(ch : str) -> int:
    score = 0
    for i in range(0, len(ch)):               #<-- remarquer le changement ici
        score = score + score_lettre(ch[i])
    return score

s = score_mot('abbabaabaaabbbaabbaba')
print(s)
print(score_mot('aaaaaaaaaabbbbbbbbbbbbbbbbbaaaaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb'))
print(score_mot('aba'))
