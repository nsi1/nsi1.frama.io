def score_lettre(car : str) -> int:
    if car == 'a':
        return 1
    else:
        return 3

def score_mot(ch: str)  -> int:
    return score_lettre(ch[0]) + score_lettre(ch[1]) + score_lettre(ch[2]) + score_lettre(ch[3])


s = score_mot('babb')
print(s)