def prix_ttc(ht: float, tva: float) -> float:
        ttc = ht * ( 1 + tva / 100)
        return ttc

prix = 200
prix_tva = prix_ttc(prix,20)
print(prix,"euros hors taxe soit",prix_tva,"euros ttc.")