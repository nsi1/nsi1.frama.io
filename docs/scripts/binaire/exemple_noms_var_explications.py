import unicodedata as u

ο = 2
о = 3
ο = ο + 1
print(ο)

print("ο des lignes 3, 5 et 6", u.name('ο'))
print("ο de la ligne 4", u.name('о'))